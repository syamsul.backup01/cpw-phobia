--[[
zhangyitian 20160622
目前用于任务完成触发播放gfx光效
将来可用于其它事件触发其它行为
--]]

event_hint = {};

event_hint[1] =
{
	task_id = 36566,
	gfx_path = "界面\\四季_春季.gfx",
	gfx_pos_x = 512,
	gfx_pos_y = 384,
}

event_hint[2] =
{
	task_id = 36567,
	gfx_path = "界面\\四季_夏季.gfx",
	gfx_pos_x = 512,
	gfx_pos_y = 384,
}

event_hint[3] =
{
	task_id = 36568,
	gfx_path = "界面\\四季_秋季.gfx",
	gfx_pos_x = 512,
	gfx_pos_y = 384,
}

event_hint[4] =
{
	task_id = 36569,
	gfx_path = "界面\\四季_冬季.gfx",
	gfx_pos_x = 512,
	gfx_pos_y = 384,
}

event_hint[5] =
{
	task_id = 36689,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 1,
}
event_hint[6] =
{
	task_id = 36690,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 2,
}
event_hint[7] =
{
	task_id = 36721,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 3,
}
event_hint[8] =
{
	task_id = 36722,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 4,
}
event_hint[9] =
{
	task_id = 36821,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 5,
}
event_hint[10] =
{
	task_id = 36822,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 6,
}
event_hint[11] =
{
	task_id = 36933,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 7,
}
event_hint[12] =
{
	task_id = 36934,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 8,
}
event_hint[13] =
{
	task_id = 36937,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 9,
}
event_hint[14] =
{
	task_id = 36938,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 10,
}
event_hint[15] =
{
	task_id = 36943,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 11,
}
event_hint[16] =
{
	task_id = 36944,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 12,
}
event_hint[17] =
{
	task_id = 36945,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title =-1,
	talk_text = 13,
}