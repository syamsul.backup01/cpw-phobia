--[[
FileName: systemteach.lua
Author: Mazongyang
Data: 2018.7.20
Description: 成长目标系统(新手引导)配置表
             包含成长目标界面&系统教学界面配置
             .lua格式的文件须使用utf-8编码 
--]]
------------------------------------------------------------------------------------------------------------
--成长目标界面配置表
growth_target = 
{
	-- 位于Configs\\成长目标配置表 目录下的模板ID
  5237,5238,5239,5240,5241,5242,5243,5244,5245,5252,5253,5254,5255,5256,5257,5258,5259,5260,5261,5262,5263,5264,5265,5266,5267,5268,5269,5270,5271,
  5272,5273,5274,5275,5276,5277,5278,5279,5280,5281,5282,5283,5284,5285,5286,5287,5288,5289,5290,5291,5292,
  5298,5304,5310,5312,5314,5315,5316,5317,5318,5319,5320,5321,5322,5323,5324,5325,5326,5327,5328,5329,5330,
  5331,5332,5333,5334,
  5335,5336,5337,5338,5339,5340,5341,5342,5343,5344,5345,5346,5347,5348,5349,5350,5351,5352,5353,5354,5355,5356,
  5357,5358,5359,5360,5361,5363,5364,5365,5366,5367,5368,5369,5370,5371,5372,5373,5374,5375,5376,
  5377,5378,5379,5380,5381,5382,5383,5384,5385,5386,5387,5388,5389,5390,5391,5392,5393,5395
}


------------------------------------------------------------------------------------------------------------
--等级弹出
level_pop = 
{
   [1] = 
   {
     ["reincarnation_level"] = 0, --转生等级，取值范围[0,2]
   ["level"] = 20,               --等级[1,105]
   },

}

------------------------------------------------------------------------------------------------------------

--系统教学界面配置表 
system_teach = {} 

--分类页签 
system_teach["1"] =   -------------------------------------------------------------------------101-------------------------------------------------------------------------------
{
    ["category"] = 1,  --分类页签名字(变强)

    ["category_label_1"] =    --系统标签
    {
       ["label_name"] = 101,   --标签名字(装备)

       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 1101,      --名字(装备精炼)
          ["intro_content"]= 10001,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\equiprefine.tga",      --图文介绍资源路径
          ["npc_service_id"] = 15,    --功能入口(NPC服务ID,若为简单服务则填1-32的整数)
          ["simple_service"] = 1,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =              --所需道具(模板ID)
          {
              ["props_1"] = 59663, --幻仙石
              ["props_2"] = 15042, --五星龙珠
              ["props_3"] = 12750, --天罡石
              ["props_4"] = 12751, --地煞石
          },
  
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 1102,   --名字(装备精炼转移)
          ["intro_content"]= 10002,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\refinetrans.tga",   --图文介绍资源路径
          ["npc_service_id"] = 17,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 1,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 12980, --乾坤石
              ["props_2"] = 12808, --天人合一符
          },
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_3"] =    --具体介绍内容
       {
          ["intro_name"] = 1103,   --名字(天书精炼)
          ["intro_content"]= 10003,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\bookrefine.tga",   --图文介绍资源路径
          ["npc_service_id"] = 7,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 3,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 59358, --幻仙谱
              ["props_2"] = 59351, --五仙玄章
              ["props_3"] = 59349, --天罡谱
              ["props_4"] = 59350, --地煞谱
          },
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_4"] =    --具体介绍内容
       {
          ["intro_name"] = 1104,   --名字(魂石融合)
          ["intro_content"]= 10004,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\enchase.tga",   --图文介绍资源路径
          ["npc_service_id"] = 2086,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 58680, --焚霞之石
              ["props_2"] = 58681, --冷萃之石
              ["props_3"] = 38155, --静岳之石
              ["props_4"] = 21043, --天工符纹
          },
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_5"] =    --具体介绍内容
       {
          ["intro_name"] = 1105,   --名字(天人合一)
          ["intro_content"]= 10005,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\equipbind.tga",   --图文介绍资源路径
          ["npc_service_id"] = 12333,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 12808, --天人合一符
   
          },
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_6"] =    --具体介绍内容
       {
          ["intro_name"] = 1106,   --名字(镌刻)
          ["intro_content"]= 10006,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\engrave.tga",   --图文介绍资源路径
          ["npc_service_id"] = 59661,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 31957, --灵媒
              ["props_2"] = 39872, --五铢钱
              ["props_3"] = 39873, --完美·五铢钱
              ["props_4"] = 59660, --生灵刻刀
          },
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

	},

    ["category_label_2"] =    -------------------------------------------------------------------------102-------------------------------------------------------------------------------
     {
       ["label_name"] = 102,   --标签名字(黄昏古魂)
       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 1201,   --名字(获取)
          ["intro_content"]= 10007,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\equipsoul_get.tga",   --图文介绍资源路径
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 59974, --沉睡的圣栽之魂
              ["props_2"] = 59975, --沉睡的圣栽之魂
              ["props_3"] = 59976, --沉睡的圣栽之魂
              ["props_4"] = 59977, --沉睡的圣栽之魂
          },
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

              ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 1202,   --名字(唤醒)
          ["intro_content"]= 10008,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\equipsoul_detect.tga",   --图文介绍资源路径
          ["demo_dlg_name"] = "HelpName_Win_Inventory",  --查看教学演示(对话框名称)
       },

       ["introduction_3"] =    --具体介绍内容
       {
          ["intro_name"] = 1203,   --名字(拆分)
          ["intro_content"]= 10009,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\equipsoul_resolve.tga",   --图文介绍资源路径
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 60000, --灵魂碎片
          },
          ["demo_dlg_name"] = "HelpName_Win_Inventory",  --查看教学演示(对话框名称)
       },

       ["introduction_4"] =    --具体介绍内容
       {
          ["intro_name"] = 1204,   --名字(萃取)
          ["intro_content"]= 10010,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\equipsoul_transfer.tga",   --图文介绍资源路径
          ["demo_dlg_name"] = "HelpName_Win_Inventory",  --查看教学演示(对话框名称)
       },

              ["introduction_5"] =    --具体介绍内容
       {
          ["intro_name"] = 1205,   --名字(涤魂)
          ["intro_content"]= 10011,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\equipsoul_refresh.tga",   --图文介绍资源路径
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 60019, --灵魂枷锁
              ["props_2"] = 60000, --灵魂碎片
          },
          ["demo_dlg_name"] = "HelpName_Win_Inventory",  --查看教学演示(对话框名称)
       },


    }, 

    ["category_label_3"] =    ---------------------------------------------------------------------------103-----------------------------------------------------------------------------
    {
       ["label_name"] = 103,   --标签名字(月之流晶)
       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 1301,   --名字(月之流晶获取)
          ["intro_content"]= 10012,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\equipsoulstone_get.tga",   --图文介绍资源路径
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 59713, --月之流晶
          },
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 1302,   --名字(月之流晶升级)
          ["intro_content"]= 10013,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\equipsoulstone_upgrade.tga",   --图文介绍资源路径
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 59713, --月之流晶
          },
          ["demo_dlg_name"] = "HelpName_Win_Inventory",  --查看教学演示(对话框名称)
       },


    }, 

    ["category_label_4"] =    ----------------------------------------------------------------------------104----------------------------------------------------------------------------
    {
       ["label_name"] = 104,   --标签名字(星盘)
              ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 1401,   --名字(星盘获取)
          ["intro_content"]= 10014,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\astrolabeget.tga",   --图文介绍资源路径
          ["npc_id"] = 40288,    --功能入口(NPC模板ID)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 47379, --星鸿秘匣
		      },
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 1402,   --名字(星盘星噬)
          ["intro_content"]= 10015,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\astrolabeswallow.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 47377, --星魂百纳丹1级
              ["props_2"] = 47501, --星魂百纳丹2级
              ["props_3"] = 47502, --星魂百纳丹3级
          },
       },

       ["introduction_3"] =    --具体介绍内容
       {
          ["intro_name"] = 1403,   --名字(星盘占星)
          ["intro_content"]= 10016,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\astrolaberandomaddon.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 47493, --星云散聚石
              ["props_2"] = 47376, --星云散聚石
              ["props_3"] = 47494, --星云散聚石
          },
       },

       ["introduction_4"] =    --具体介绍内容
       {
          ["intro_name"] = 1404,   --名字(星盘观星)
          ["intro_content"]= 10017,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\astrolabeobserve.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 47500, --幻仙谱
          },
       },

       ["introduction_5"] =    --具体介绍内容
       {
          ["intro_name"] = 1405,   --名字(星盘星变)
          ["intro_content"]= 10018,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\astrolabeincpointvalue.tga",   --图文介绍资源路径
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 47499, --星灵奇引珠
              ["props_2"] = 47497, --星灵奇引珠3级
              ["props_3"] = 47495, --星灵奇引珠2级
              ["props_4"] = 47452, --星灵奇引珠1级
          },
       },

       ["introduction_6"] =    --具体介绍内容
       {
          ["intro_name"] = 1406,   --名字(星盘锁定)
          ["intro_content"]= 10019,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\astrolabelock.tga",   --图文介绍资源路径
       },


    }, 

    ["category_label_5"] =   -----------------------------------------------------------------------------------------105---------------------------------------------------------------
    {
       ["label_name"] = 105,   --标签名字(战灵)

              ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 1501,   --名字(装备战灵)
          ["intro_content"]= 10020,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\generalcardequip.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_GeneralCard",  --查看教学演示(对话框名称)
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 1502,   --名字(战灵升级与转生)
          ["intro_content"]= 10021,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\generalcardupgrade.tga",   --图文介绍资源路径
          ["npc_service_id"] = 14,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 2,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_3"] =    --具体介绍内容
       {
          ["intro_name"] = 1503,   --名字(战灵合成)
          ["intro_content"]= 10022,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\generalcardcompound.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 42190, --战灵宝具
              ["props_2"] = 57812, --渡魂之兆
              ["props_3"] = 57814, --铸魄灵预
          },
          ["demo_dlg_name"] = "HelpName_Win_PokerCompound",  --查看教学演示(对话框名称)
       },

       ["introduction_4"] =    --具体介绍内容
       {
          ["intro_name"] = 1504,   --名字(战灵注入帝魂)
          ["intro_content"]= 10023,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\generalcardkingsoul.tga",   --图文介绍资源路径
          ["npc_service_id"] = 57861,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 57645, --帝魂
              ["props_2"] = 42190, --战灵宝具
              ["props_3"] = 57812, --渡魂之兆
              ["props_4"] = 57814, --铸魄灵预
          },
          ["demo_dlg_name"] = "HelpName_Win_GeneralCard",  --查看教学演示(对话框名称)
       },

       ["introduction_5"] =    --具体介绍内容
       {
          ["intro_name"] = 1505,   --名字(战灵元魂)
          ["intro_content"]= 10024,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\generalcardsoul.tga",   --图文介绍资源路径
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 47521, --一阶元魂
              ["props_2"] = 47519, --一阶元魂
              ["props_3"] = 47517, --一阶元魂
              ["props_4"] = 47513, --一阶元魂
          },
          ["demo_dlg_name"] = "HelpName_Win_MonsterSpirit",  --查看教学演示(对话框名称)
       },


    }, 

    ["category_label_6"] =   ---------------------------------------------------------------------------------106-----------------------------------------------------------------------
    {
       ["label_name"] = 106,   --标签名字(小精灵)


       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 1601,   --名字(精灵升级)
          ["intro_content"]= 10025,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\elfupgrade.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_CharacterELF",  --查看教学演示(对话框名称)
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 1602,   --名字(精灵属性)
          ["intro_content"]= 10026,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\elfattribute.tga",   --图文介绍资源路径
          ["npc_service_id"] = 19,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 1,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 23552, --一元复始令
          },
          ["demo_dlg_name"] = "HelpName_Win_CharacterELF",  --查看教学演示(对话框名称)
       },

       ["introduction_3"] =    --具体介绍内容
       {
          ["intro_name"] = 1603,   --名字(精灵装备)
          ["intro_content"]= 10027,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\elfequip.tga",   --图文介绍资源路径
          ["npc_service_id"] = 23,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 1,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_CharacterELF",  --查看教学演示(对话框名称)
       },

       ["introduction_4"] =    --具体介绍内容
       {
          ["intro_name"] = 1604,   --名字(精灵天赋)
          ["intro_content"]= 10028,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\elfgift.tga",   --图文介绍资源路径
          ["npc_service_id"] = 18,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 1,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 23553, --万象更新石
          },
          ["demo_dlg_name"] = "HelpName_Win_CharacterELF",  --查看教学演示(对话框名称)
       },

       ["introduction_5"] =    --具体介绍内容
       {
          ["intro_name"] = 1605,   --名字(精灵技能)
          ["intro_content"]= 10029 ,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\elfskii.tga",   --图文介绍资源路径
          ["npc_service_id"] = 23,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 1,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_CharacterELF",  --查看教学演示(对话框名称)
       },


    }, 

    ["category_label_7"] =    ------------------------------------------------------------------------------107--------------------------------------------------------------------------
    {
       ["label_name"] = 107,   --标签名字(灵脉)

       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 1701,   --名字(灵脉升级)
          ["intro_content"]= 10030,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\meridians.tga",   --图文介绍资源路径
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 38144, --玄鉴丹
              ["props_2"] = 38147, --绝圣丹
          },
          ["demo_dlg_name"] = "HelpName_Win_Meridians",  --查看教学演示(对话框名称)
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 1702,   --名字(灵脉突破)
          ["intro_content"]= 10031,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\meridiansbreak.tga",   --图文介绍资源路径
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 38144, --玄鉴丹
              ["props_2"] = 38147, --绝圣丹
          },
          ["demo_dlg_name"] = "HelpName_Win_Meridians",  --查看教学演示(对话框名称)
       },


    }, 

    ["category_label_8"] =    -------------------------------------------------------------------------------------108-------------------------------------------------------------------
    {
       ["label_name"] = 108,   --标签名字(符纹)

       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 1801,   --名字(符纹刻位开启)
          ["intro_content"]= 10032,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\runeopen.tga",   --图文介绍资源路径
          ["npc_id"] = 54196,         --功能入口(NPC模板ID)
          ["demo_dlg_name"] = "HelpName_Win_RuneMain",  --查看教学演示(对话框名称)
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 1802,   --名字(符纹镶嵌)
          ["intro_content"]= 10033,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\runeinlay.tga",   --图文介绍资源路径
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 52169, --仁爱之证
              ["props_2"] = 52170, --荣耀之证
              ["props_3"] = 52171, --勇毅之证
              ["props_4"] = 52172, --谦逊之证
          },
          ["demo_dlg_name"] = "HelpName_Win_RuneMain",  --查看教学演示(对话框名称)
       },

       ["introduction_3"] =    --具体介绍内容
       {
          ["intro_name"] = 1803,   --名字(符纹培养)
          ["intro_content"]= 10034,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\runegrowup.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 54332, --符纹包（1级
              ["props_2"] = 54336, --符纹包（5级）
              ["props_3"] = 54338, --符纹包（7级）
              ["props_4"] = 52175, --鎏金符纹1级
          },
          ["demo_dlg_name"] = "HelpName_Win_RuneMain",  --查看教学演示(对话框名称)
       },

       ["introduction_4"] =    --具体介绍内容
       {
          ["intro_name"] = 1804,   --名字(符纹克敌属性)
          ["intro_content"]= 10035,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\runerestrain.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_RuneMain",  --查看教学演示(对话框名称)
       },

    }, 

    ["category_label_9"] =   ----------------------------------------------------------------------------------------109----------------------------------------------------------------
    {
       ["label_name"] = 109,   --标签名字(称号)

       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 1901,   --名字(称号获取)
          ["intro_content"]= 10036,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\eventtitleget.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 1902,   --名字(称号装备)
          ["intro_content"]= 10037,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\eventtitleequip.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_TitleList",  --查看教学演示(对话框名称)
       },

    }, 

["category_label_10"] =   ----------------------------------------------------------------------------------------109----------------------------------------------------------------
    {
       ["label_name"] = 110,   --标签名字(武魂宝库)

       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 1001,   --名字(获取)
          ["intro_content"]= 10076,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\voidinventoryopen.tga",   --图文介绍资源路径
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 60104, --武魂灵石

          },
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 1002,   --名字(套装搭配)
          ["intro_content"]= 10077,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\voidinventory.tga",   --图文介绍资源路径
          ["demo_dlg_name"] = "HelpName_Win_Inventory",  --查看教学演示(对话框名称)
       },

    }, 

} 



system_teach["2"] =
{
    ["category"] = 2,  --分类页签名字(社交外观)

    ["category_label_1"] =    ------------------------------------------------------------------------------------201--------------------------------------------------------------------
    {
       ["label_name"] = 201,   --标签名字(好友)
       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 2101,   --名字(添加好友)
          ["intro_content"]= 10039,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\friendadd.tga",   --图文介绍资源路径
          ["demo_dlg_name"] = "HelpName_Win_FriendList",  --查看教学演示(对话框名称)
       },

              ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 2102,   --名字(好友互动)
          ["intro_content"]= 10040,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\friendinteraction.tga",   --图文介绍资源路径
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

      

    }, 

    ["category_label_2"] =    -------------------------------------------------------------------------------------202-------------------------------------------------------------------
    {
       ["label_name"] = 202,   --标签名字(帮派)
       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 2201,   --名字(帮派加入)
          ["intro_content"]= 10041,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\guildinter.tga",   --图文介绍资源路径
          ["npc_service_id"] = 5,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 1,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_GuildSearch",  --查看教学演示(对话框名称)
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 2202,   --名字(帮派福利)
          ["intro_content"]= 10042,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\guildaward.tga",   --图文介绍资源路径
          ["npc_id"] = 57220,         --功能入口(NPC模板ID)
          ["demo_dlg_name"] = "HelpName_Win_GuildManage",  --查看教学演示(对话框名称)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 38476, --饺子
              ["props_2"] = 38475, --汤圆
              ["props_3"] = 57840, --护身符
              ["props_4"] = 57841, --守神符
          },
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_3"] =    --具体介绍内容
       {
          ["intro_name"] = 2203,   --名字(帮派活动)
          ["intro_content"]= 10043,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\guildactivity.tga",   --图文介绍资源路径
          ["npc_id"] = 57680,         --功能入口(NPC模板ID)
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_4"] =    --具体介绍内容
       {
          ["intro_name"] = 2204,   --名字(帮派基地)
          ["intro_content"]= 10044,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\guildbase.tga",   --图文介绍资源路径
          ["npc_id"] = 29950,         --功能入口(NPC模板ID)
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },



    }, 

    ["category_label_3"] =    ---------------------------------------------------------------------------------------203-----------------------------------------------------------------
    {
       ["label_name"] = 203,   --标签名字(师徒)
              ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 2301,   --名字(拜师)
          ["intro_content"]= 10045,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\master.tga",   --图文介绍资源路径
          ["demo_dlg_name"] = "HelpName_Win_Masearch1",  --查看教学演示(对话框名称)
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 2302,   --名字(师徒互动)
          ["intro_content"]= 10046,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\masterinteraction.tga",   --图文介绍资源路径
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_3"] =    --具体介绍内容
       {
          ["intro_name"] = 2303,   --名字(师徒副本)
          ["intro_content"]= 10047,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\masterinstance.tga",   --图文介绍资源路径
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_4"] =    --具体介绍内容
       {
          ["intro_name"] = 2304,   --名字(出师)
          ["intro_content"]= 10048,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\graduation.tga",   --图文介绍资源路径
          ["npc_id"] = 60169,      --功能入口(NPC模板ID)月老
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_5"] =    --具体介绍内容
       {
          ["intro_name"] = 2305,   --名字(收徒)
          ["intro_content"]= 10049,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\findstudent.tga",   --图文介绍资源路径
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

    }, 

    ["category_label_4"] =   -------------------------------------------------------------------------------------204-------------------------------------------------------------------
    {
       ["label_name"] = 204,   --标签名字(夫妻)

              ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 2401,   --名字(结婚)
          ["intro_content"]= 10050,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\marriage.tga",   --图文介绍资源路径
          ["npc_id"] = 12398,      --功能入口(NPC模板ID)月老
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 16169, --新娘大礼包
              ["props_2"] = 16168, --新郎大礼包
          },
          ["demo_dlg_name"] = "HelpName_Win_CoupleMarriage",  --查看教学演示(对话框名称)
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 2402,   --名字(夫妻互动)
          ["intro_content"]= 10051,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\coupleinteraction.tga",   --图文介绍资源路径
          ["npc_id"] = 26754,      --功能入口(NPC模板ID)月老侍童
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_3"] =    --具体介绍内容
       {
          ["intro_name"] = 2403,   --名字(夫妻副本)
          ["intro_content"]= 10052,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\coupleinstance.tga",   --图文介绍资源路径
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

       ["introduction_4"] =    --具体介绍内容
       {
          ["intro_name"] = 2404,   --名字(离婚)
          ["intro_content"]= 10053,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\divorce.tga",   --图文介绍资源路径
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },


    }, 


} 


system_teach["3"] =
{
    ["category"] = 3,  --分类页签名字(外观)
    ["category_label_1"] =    ------------------------------------------------------------------------------------301--------------------------------------------------------------------
    {
       ["label_name"] = 301,   --标签名字(时装)
       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 3101,   --名字(时装获取)
          ["intro_content"]= 10054,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\clothget.tga",   --图文介绍资源路径
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 3102,   --名字(时装染色)
          ["intro_content"]= 10055,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\clothcolor.tga",   --图文介绍资源路径
          ["npc_service_id"] = 16,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 1,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 15312, --纯白色染色剂
              ["props_2"] = 15579, --石榴红染色剂
              ["props_3"] = 15314, --纯黑色染色剂
              ["props_4"] = 15590, --浅海沙染色剂
          },
          ["demo_dlg_name"] = "HelpName_Win_FittingRoom",  --查看教学演示(对话框名称)
       },

       ["introduction_3"] =    --具体介绍内容
       {
          ["intro_name"] = 3103,   --名字(时装模式)
          ["intro_content"]= 10078,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\clothstyle.tga",   --图文介绍资源路径
       },


    }, 

    ["category_label_2"] =    -------------------------------------------------------------------------------------302-------------------------------------------------------------------
    {
       ["label_name"] = 302,   --标签名字(宠物)
       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 3201,   --名字(宠物背包位扩展)
          ["intro_content"]= 10056,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\petbag.tga",   --图文介绍资源路径
          ["npc_service_id"] = 10,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 1,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 12768, --宠物笼
          },
          ["demo_dlg_name"] = "HelpName_Win_PetList",  --查看教学演示(对话框名称)
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 3202,   --名字(宠物观赏宠)
          ["intro_content"]= 10057,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\ornamentalpet.tga",   --图文介绍资源路径
          ["npc_service_id"] = 11,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 1,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_PetList",  --查看教学演示(对话框名称)
       },

       ["introduction_3"] =    --具体介绍内容
       {
          ["intro_name"] = 3203,   --名字(宠物骑宠)
          ["intro_content"]= 10058,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\ridepet.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 8796, --清水
              ["props_2"] = 16157, --狂风靴
              ["props_3"] = 15757, --疾风靴
          },
          ["demo_dlg_name"] = "HelpName_Win_PetList",  --查看教学演示(对话框名称)
       },

       ["introduction_4"] =    --具体介绍内容
       {
          ["intro_name"] = 3204,   --名字(宠物战宠)
          ["intro_content"]= 10059,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\fightpet.tga",   --图文介绍资源路径
          ["npc_service_id"] = 12283,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_PetList",  --查看教学演示(对话框名称)
       },



    }, 

    ["category_label_3"] =    ---------------------------------------------------------------------------------------303-----------------------------------------------------------------
    {
       ["label_name"] = 303,   --标签名字(飞行器)
              ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 3301,   --名字(飞行器加速飞行)
          ["intro_content"]= 10060,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\flightaccelerate.tga",   --图文介绍资源路径
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 5639, --完美石
          },
          ["demo_dlg_name"] = "HelpName_Win_Inventory",  --查看教学演示(对话框名称)
       },

       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 3302,   --名字(飞行器加速合成)
          ["intro_content"]= 10061,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\flightupgrade.tga",   --图文介绍资源路径
          ["npc_id"] = 19223,      --功能入口(NPC模板ID)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 42849, --飞羽之翼
              ["props_2"] = 42838, --伏羲珠
              ["props_3"] = 43024, --荧光之翼
              ["props_4"] = 19203, --风之精灵
          },
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

    }, 


} 

system_teach["4"] =
{
    ["category"] = 4,  --分类页签名字(乾坤袋)

    ["category_label_1"] =    ------------------------------------------------------------------------------------401--------------------------------------------------------------------
    {
       ["label_name"] = 401,   --标签名字(首充)
       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 4101,   --名字(首充)
          ["intro_content"]= 10062,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\firstrecharge.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_Rechargegift",  --查看教学演示(对话框名称)
       },      

    }, 

    ["category_label_2"] =    -------------------------------------------------------------------------------------402-------------------------------------------------------------------
    {
       ["label_name"] = 402,   --标签名字(竞拍大厅)
       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 4201,   --名字(竞拍大厅)
          ["intro_content"]= 10063,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\auction.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
       },

    }, 

    --["category_label_3"] =    ---------------------------------------------------------------------------------------403-----------------------------------------------------------------
    --{
     --  ["label_name"] = 403,   --标签名字(新手专署)
     --         ["introduction_1"] =    --具体介绍内容
     --  {
      --    ["intro_name"] = 4301,   --名字(新手专署)
      --    ["intro_content"]= 10064,        --描述内容
      --    ["picture_path"] = "Surfaces\\version02\\teach\\fornewcomer.tga",   --图文介绍资源路径
      --    ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
       --   ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
      -- },

   -- }, 

    ["category_label_3"] =    ---------------------------------------------------------------------------------------404-----------------------------------------------------------------
    {
       ["label_name"] = 404,   --标签名字(万宝奇货)
              ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 4401,   --名字(万宝奇货)
          ["intro_content"]= 10065,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\qshop.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
       },

    }, 

    ["category_label_4"] =    ---------------------------------------------------------------------------------------405-----------------------------------------------------------------
    {
       ["label_name"] = 405,   --标签名字(vip乾坤袋)
              ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 4501,   --名字(vip乾坤袋)
          ["intro_content"]= 10066,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\vipshop.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
       },

    }, 


} 

system_teach["5"] =
{
    ["category"] = 5,  --分类页签名字(辅助)
    ["category_label_1"] =    ------------------------------------------------------------------------------------501--------------------------------------------------------------------
    {
       ["label_name"] = 501,   --标签名字(点石成金)
              ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 5101,   --名字(点石成金)
          ["intro_content"]= 10067,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\recycle.tga",   --图文介绍资源路径
          ["npc_service_id"] = 56232,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },      

    }, 

    ["category_label_2"] =    -------------------------------------------------------------------------------------502-------------------------------------------------------------------
    {
       ["label_name"] = 502,   --标签名字(自动修炼)
       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 5201,   --名字(连续技)
          ["intro_content"]= 10068,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\continuityskill.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_SkillAction",  --查看教学演示(对话框名称)
       },
       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 5202,   --名字(自动修炼)
          ["intro_content"]= 10069,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\autopolicy.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_AutoPolicy",  --查看教学演示(对话框名称)
       },

    }, 

    ["category_label_3"] =    ---------------------------------------------------------------------------------------503-----------------------------------------------------------------
    {
       ["label_name"] = 503,   --标签名字(游戏助手)
       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 5301,   --名字(游戏助手打开方式)
          ["intro_content"]= 10070,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\gameassistant.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },
       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 5302,   --名字(游戏助手功能介绍)
          ["intro_content"]= 10071,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\gameassistantintroduce.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },

    }, 

    ["category_label_4"] =    ---------------------------------------------------------------------------------------504-----------------------------------------------------------------
    {
       ["label_name"] = 504,   --标签名字(凌云界)
       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 5401,   --名字(凌云界开启和介绍)
          ["intro_content"]= 10072,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\homeopen.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },
       ["introduction_2"] =    --具体介绍内容
       {
          ["intro_name"] = 5402,   --名字(凌云界种植生产)
          ["intro_content"]= 10073,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\homeplant.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 50411, --小麦种子
              ["props_2"] = 50417, --人参果种子
              ["props_3"] = 50418, --兔子幼崽
              ["props_4"] = 50424, --猪幼崽
          },
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },
       ["introduction_3"] =    --具体介绍内容
       {
          ["intro_name"] = 5403,   --名字(凌云界神兽对战)
          ["intro_content"]= 10074,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\homepetfight.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["props_id"] =           --所需道具(模板ID)
          {
              ["props_1"] = 51758, --幻仙谱
          },
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },
       ["introduction_4"] =    --具体介绍内容
       {
          ["intro_name"] = 5404,   --名字(凌云界天工开物坊)
          ["intro_content"]= 10075,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\homeworkshop.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "",  --查看教学演示(对话框名称)
       },
    }, 

    ["category_label_5"] =   ----------------------------------------------------------------------------------------505----------------------------------------------------------------
    {
       ["label_name"] = 505,   --标签名字(角色模拟器)

       ["introduction_1"] =    --具体介绍内容
       {
          ["intro_name"] = 5501,   --名字(角色模拟器)
          ["intro_content"]= 10038,        --描述内容
          ["picture_path"] = "Surfaces\\version02\\teach\\simulator.tga",   --图文介绍资源路径
          ["npc_service_id"] = 0,    --功能入口(NPC服务ID,若为简单服务则填0-31的整数)
          ["simple_service"] = 0,     --是否简单服务(0为非简单服务,填写1、2、3为简单服务)
          ["demo_dlg_name"] = "HelpName_Win_SimulatorMain",  --查看教学演示(对话框名称)
       },

    }, 


} 