--LuaWrapper无法处理表循环调用, 故使用module表,必须删除_M自身引用
local function FilterSelfQuote(_varName)
    if _G[_varName] and _G[_varName] == _G[_varName]._M then
        _G[_varName]._M = nil
    end
end


--获取打包脚本配置
function glb_GetScriptConfigTable(_varName)
    FilterSelfQuote(_varName)
    return _G[_varName]
end
