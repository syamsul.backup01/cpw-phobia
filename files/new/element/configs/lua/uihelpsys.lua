--[[
yongjiang.yang 20181012
界面帮助系统配置
配置了本文件之外还需要在ingame.dcf中添加文件的名称和位置信息

"main_dialog": 	该项配置表示该系统的主界面名称，是dialog 名称不是文件名
"help_x":		该项配置表示该系统的帮助界面名称，是dialog 名称不是文件名
--]]


ui_help_system = {}

ui_help_system[1] =
{
	["main_dialog"] = "Win_TitleList",
	["help_0"] = "Win_TitleListHelp1",
	["help_2"] = "Win_TitleListHelp3",
	["help_1"] = "Win_TitleListHelp2",
	["help_3"] = "Win_TitleListHelp4",
	["help_4"] = "Win_TitleListHelp5",
}
ui_help_system[2] =
{
	["main_dialog"] = "Win_SimulatorMain",
	["help_0"] = "Win_SimulatorMainHelp1",
	["help_1"] = "Win_SimulatorMainHelp2",
	["help_2"] = "Win_SimulatorMainHelp3",
	["help_3"] = "Win_SimulatorMainHelp4",
}
ui_help_system[3] =
{
	["main_dialog"] = "Win_GeneralCard",
	["help_0"] = "Win_GeneralCardHelp1",
	["help_1"] = "Win_GeneralCardHelp2",
	["help_2"] = "Win_GeneralCardHelp3",
	["help_3"] = "Win_GeneralCardHelp4",
	["help_4"] = "Win_GeneralCardHelp5",
}
ui_help_system[4] =
{
	["main_dialog"] = "Win_RuneMain",
	["help_0"] = "Win_skillrune_help1",
	["help_1"] = "Win_skillrune_help2",
	["help_2"] = "Win_skillrune_help3",
	["help_3"] = "Win_skillrune_help4",
}
ui_help_system[5] =
{
	["main_dialog"] = "Win_EngraveNew",
	["help_0"] = "Win_engravenew_help1",
	["help_1"] = "Win_engravenew_help2",
	["help_2"] = "Win_engravenew_help3",
	["help_3"] = "Win_engravenew_help4",
}
ui_help_system[6] =
{
	["main_dialog"] = "Win_CharacterELF",
	["help_0"] = "Win_character_help1",
	["help_1"] = "Win_character_help2",
	["help_2"] = "Win_character_help3",
	["help_3"] = "Win_character_help4",
}
ui_help_system[7] =
{
	["main_dialog"] = "Win_EquipSoul_Spirit",
	["help_0"] = "Win_EquipSoul_SpiritHelp1",
	["help_1"] = "Win_EquipSoul_SpiritHelp2",
	["help_2"] = "Win_EquipSoul_SpiritHelp3",
	["help_3"] = "Win_EquipSoul_SpiritHelp4",
}