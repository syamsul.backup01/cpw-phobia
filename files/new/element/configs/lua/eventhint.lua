--[[
zhangyitian 20160622
目前用于任务完成触发播放gfx光效
将来可用于其它事件触发其它行为
--]]

event_hint = {};

event_hint[1] ={task_id = 36566,gfx_path = "界面\\四季_春季.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}

event_hint[2] =
{
	task_id = 36567,
	gfx_path = "界面\\四季_夏季.gfx",
	gfx_pos_x = 512,
	gfx_pos_y = 384,
}

event_hint[3] =
{
	task_id = 36568,
	gfx_path = "界面\\四季_秋季.gfx",
	gfx_pos_x = 512,
	gfx_pos_y = 384,
}

event_hint[4] =
{
	task_id = 36569,
	gfx_path = "界面\\四季_冬季.gfx",
	gfx_pos_x = 512,
	gfx_pos_y = 384,
}

event_hint[5] =
{
	task_id = 36689,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 1,
}
event_hint[6] =
{
	task_id = 36690,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 2,
}
event_hint[7] =
{
	task_id = 36725,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 3,
}
event_hint[8] =
{
	task_id = 36722,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 4,
}
event_hint[9] =
{
	task_id = 36821,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 5,
}
event_hint[10] =
{
	task_id = 36822,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 6,
}
event_hint[11] =
{
	task_id = 36933,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 7,
}
event_hint[12] =
{
	task_id = 36934,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 8,
}
event_hint[13] =
{
	task_id = 36937,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 9,
}
event_hint[14] =
{
	task_id = 36938,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 10,
}
event_hint[15] =
{
	task_id = 36943,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 11,
}
event_hint[16] =
{
	task_id = 36944,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 12,
}
event_hint[17] =
{
	task_id = 36948,
	talk_npc_icon = "npc头像\\空.dds",
	talk_title = -1,
	talk_text = 13,
}
event_hint[19] =
{
	task_id = 37713,
	talk_npc_icon = "npc头像\\小脚丫.dds",
	talk_title = 14,
	talk_text = 15,
}
event_hint[20] =
{
	task_id = 37714,
	talk_npc_icon = "npc头像\\小脚丫.dds",
	talk_title = 14,
	talk_text = 16,
}
event_hint[21] =
{
	task_id = 37715,
	talk_npc_icon = "npc头像\\小脚丫.dds",
	talk_title = 14,
	talk_text = 17,
}

event_hint[22] =
{
	task_id =39803,
    fools_day_box = 1,
}

event_hint[23] =
{
	task_id =39809,
    fools_day_box = 1,
}

event_hint[24] =
{
	task_id =39813,
    fools_day_box = 1,
}


event_hint[3729101] ={task_id = 37291,talk_npc_icon = "npc头像\\白帝城女法师.dds",talk_title = 20161101,talk_text = 3729101,}
event_hint[3729201] ={task_id = 37292,talk_npc_icon = "npc头像\\白帝城女法师.dds",talk_title = 20161101,talk_text = 3729201,}
event_hint[3729301] ={task_id = 37293,talk_npc_icon = "npc头像\\白帝城平民05.dds",talk_title = 20161102,talk_text = 3729301,}
event_hint[3774201] ={task_id = 37742,talk_npc_icon = "npc头像\\白帝城平民05.dds",talk_title = 20161102,talk_text = 3774201,}
event_hint[3722601] ={task_id = 37226,talk_npc_icon = "npc头像\\白帝城小怪01.dds",talk_title = 20161104,talk_text = 3722601,}
event_hint[3730001] ={task_id = 37300,talk_npc_icon = "npc头像\\白帝城小怪05.dds",talk_title = 20161105,talk_text = 3730001,}
event_hint[3730901] ={task_id = 37309,talk_npc_icon = "npc头像\\白帝城平民06.dds",talk_title = 20161114,talk_text = 3730901,next_hint = 3730902}
event_hint[3730902] ={talk_npc_icon = "npc头像\\白帝城平民01.dds",talk_title = 20161113,talk_text = 3730902,next_hint = 3730903,}
event_hint[3730903] ={talk_npc_icon = "npc头像\\白帝城平民06.dds",talk_title = 20161114,talk_text = 3730903,next_hint = 3730904,}
event_hint[3730904] ={talk_npc_icon = "npc头像\\白帝城平民01.dds",talk_title = 20161113,talk_text = 3730904,next_hint = 3730905,}
event_hint[3730905] ={talk_npc_icon = "npc头像\\白帝城平民06.dds",talk_title = 20161114,talk_text = 3730905,next_hint = 3730906,}
event_hint[3730906] ={talk_npc_icon = "npc头像\\白帝城平民01.dds",talk_title = 20161113,talk_text = 3730906,next_hint = 3730907,}
event_hint[3730907] ={talk_npc_icon = "npc头像\\白帝城平民06.dds",talk_title = 20161114,talk_text = 3730907,next_hint = 3730908,}
event_hint[3730908] ={talk_npc_icon = "npc头像\\白帝城平民01.dds",talk_title = 20161113,talk_text = 3730908,}
event_hint[3723501] ={task_id = 37235,talk_npc_icon = "npc头像\\夏风将军.dds",talk_title = 20161108,talk_text = 3723501,}
event_hint[3733301] ={task_id = 37333,talk_npc_icon = "npc头像\\白帝城小怪07.dds",talk_title = 20161109,talk_text = 3733301,}
event_hint[3731301] ={task_id = 37313,talk_npc_icon = "npc头像\\玫瑰骑士团战士.dds",talk_title = 20161110,talk_text = 3731301,}
event_hint[3725501] ={task_id = 37255,talk_npc_icon = "npc头像\\玫瑰团长.dds",talk_title = 20161111,talk_text = 3725501,next_hint = 3725502,}
event_hint[3725502] ={talk_npc_icon = "npc头像\\玫瑰副团长.dds",talk_title = 20161112,talk_text = 3725502,next_hint = 3725503,}
event_hint[3725503] ={talk_npc_icon = "npc头像\\玫瑰团长.dds",talk_title = 20161111,talk_text = 3725503,next_hint = 3725504,}
event_hint[3725504] ={talk_npc_icon = "npc头像\\玫瑰副团长.dds",talk_title = 20161112,talk_text = 3725504,next_hint = 3725505,}
event_hint[3725505] ={talk_npc_icon = "npc头像\\玫瑰团长.dds",talk_title = 20161111,talk_text = 3725505,next_hint = 3725506,}
event_hint[3725506] ={talk_npc_icon = "npc头像\\玫瑰副团长.dds",talk_title = 20161112,talk_text = 3725506,next_hint = 3725507,}
event_hint[3725507] ={talk_npc_icon = "npc头像\\玫瑰团长.dds",talk_title = 20161111,talk_text = 3725507,}
event_hint[3733001] ={task_id = 37330,talk_npc_icon = "npc头像\\吟游诗人.dds",talk_title = 20161116,talk_text = 3733001,next_hint = 3733002,}
event_hint[3733002] ={talk_npc_icon = "npc头像\\卖花女.dds",talk_title = 20161117,talk_text = 3733002,next_hint = 3733003,}
event_hint[3733003] ={talk_npc_icon = "npc头像\\吟游诗人.dds",talk_title = 20161116,talk_text = 3733003,next_hint = 3733004,}
event_hint[3733004] ={talk_npc_icon = "npc头像\\卖花女.dds",talk_title = 20161117,talk_text = 3733004,next_hint = 3733005,}
event_hint[3733005] ={talk_npc_icon = "npc头像\\吟游诗人.dds",talk_title = 20161116,talk_text = 3733005,next_hint = 3733006,}
event_hint[3733006] ={talk_npc_icon = "npc头像\\卖花女.dds",talk_title = 20161117,talk_text = 3733006,next_hint = 3733007,}
event_hint[3733007] ={talk_npc_icon = "npc头像\\吟游诗人.dds",talk_title = 20161116,talk_text = 3733007,next_hint = 3733008,}
event_hint[3733008] ={talk_npc_icon = "npc头像\\卖花女.dds",talk_title = 20161117,talk_text = 3733008,}
event_hint[3733101] ={task_id = 37331,talk_npc_icon = "npc头像\\夏风将军.dds",talk_title = 20161108,talk_text = 3733101,}
event_hint[3726601] ={task_id = 37266,talk_npc_icon = "npc头像\\白帝城平民05.dds",talk_title = 20161102,talk_text = 3726601,}
event_hint[3734501] ={task_id = 37345,talk_npc_icon = "npc头像\\白帝城平民05.dds",talk_title = 20161102,talk_text = 3734501,}
event_hint[3734801] ={task_id = 37348,talk_npc_icon = "npc头像\\白帝城平民02.dds",talk_title = 20161106,talk_text = 3734801,next_hint = 3734802,}
event_hint[3734802] ={talk_npc_icon = "npc头像\\白帝城平民03.dds",talk_title = 20161107,talk_text = 3734802,next_hint = 3734803,}
event_hint[3734803] ={talk_npc_icon = "npc头像\\白帝城平民02.dds",talk_title = 20161106,talk_text = 3734803,next_hint = 3734804,}
event_hint[3734804] ={talk_npc_icon = "npc头像\\白帝城平民03.dds",talk_title = 20161107,talk_text = 3734804,next_hint = 3734805,}
event_hint[3734805] ={talk_npc_icon = "npc头像\\白帝城平民02.dds",talk_title = 20161106,talk_text = 3734805,next_hint = 3734806,}
event_hint[3734806] ={talk_npc_icon = "npc头像\\白帝城平民03.dds",talk_title = 20161107,talk_text = 3734806,}
event_hint[3776301] ={task_id = 37763,talk_npc_icon = "npc头像\\白帝城小怪04.dds",talk_title = 20161118,talk_text = 3776301,}
event_hint[3728201] ={task_id = 37282,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3728201,}
event_hint[3728501] ={task_id = 37285,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3728501,}
event_hint[3728701] ={task_id = 37287,talk_npc_icon = "npc头像\\玫瑰副团长.dds",talk_title = 20161112,talk_text = 3728701,next_hint = 3728702,}
event_hint[3728702] ={talk_npc_icon = "npc头像\\玫瑰团长.dds",talk_title = 20161111,talk_text = 3728702,next_hint = 3728703,}
event_hint[3728703] ={talk_npc_icon = "npc头像\\玫瑰副团长.dds",talk_title = 20161112,talk_text = 3728703,next_hint = 3728704,}
event_hint[3728704] ={talk_npc_icon = "npc头像\\玫瑰团长.dds",talk_title = 20161111,talk_text = 3728704,next_hint = 3728705,}
event_hint[3728705] ={talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3728705,}
event_hint[3728801] ={task_id = 37288,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3728801,}
event_hint[3700701] ={task_id = 37007,talk_npc_icon = "npc头像\\费空行.dds",talk_title = 20161120,talk_text = 3700701,}
event_hint[3701301] ={task_id = 37013,talk_npc_icon = "npc头像\\费空行.dds",talk_title = 20161120,talk_text = 3701301,}
event_hint[3701401] ={task_id = 37014,talk_npc_icon = "npc头像\\费空行.dds",talk_title = 20161120,talk_text = 3701401,}
event_hint[3701701] ={task_id = 37017,talk_npc_icon = "npc头像\\费空行.dds",talk_title = 20161120,talk_text = 3701701,}
event_hint[3701901] ={task_id = 37019,talk_npc_icon = "npc头像\\费空行.dds",talk_title = 20161120,talk_text = 3701901,next_hint = 3701902,}
event_hint[3701902] ={talk_npc_icon = "npc头像\\荆无命.dds",talk_title = 20161121,talk_text = 3701902,next_hint = 3701903,}
event_hint[3701903] ={talk_npc_icon = "npc头像\\费空行.dds",talk_title = 20161120,talk_text = 3701903,}
event_hint[3703901] ={task_id = 37039,talk_npc_icon = "npc头像\\荆无命.dds",talk_title = 20161121,talk_text = 3703901,}
event_hint[3700801] ={task_id = 37008,talk_npc_icon = "npc头像\\荆无命.dds",talk_title = 20161121,talk_text = 3700801,}
event_hint[3700901] ={task_id = 37009,talk_npc_icon = "npc头像\\荆无命.dds",talk_title = 20161121,talk_text = 3700901,}
event_hint[3701201] ={task_id = 37012,talk_npc_icon = "npc头像\\荆无命.dds",talk_title = 20161121,talk_text = 3701201,}
event_hint[3704101] ={task_id = 37041,talk_npc_icon = "npc头像\\荆无命.dds",talk_title = 20161121,talk_text = 3704101,next_hint = 3704102,}
event_hint[3704102] ={talk_npc_icon = "npc头像\\费空行.dds",talk_title = 20161120,talk_text = 3704102,next_hint = 3704103,}
event_hint[3704103] ={talk_npc_icon = "npc头像\\荆无命.dds",talk_title = 20161121,talk_text = 3704103,}
event_hint[3707501] ={task_id = 37075,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20161122,talk_text = 3707501,}
event_hint[3709001] ={task_id = 37090,talk_npc_icon = "npc头像\\白帝城小怪06.dds",talk_title = 20161123,talk_text = 3709001,}
event_hint[3777001] ={task_id = 37770,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3777001,}
event_hint[3775701] ={task_id = 37757,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3775701,}
event_hint[3740501] ={task_id = 37405,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3740501,}
event_hint[3740601] ={task_id = 37406,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3740601,}
event_hint[3735901] ={task_id = 37359,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3735901,}
event_hint[3736301] ={task_id = 37363,talk_npc_icon = "npc头像\\玫瑰副团长.dds",talk_title = 20161112,talk_text = 3736301,next_hint = 3736302,}
event_hint[3736302] ={talk_npc_icon = "npc头像\\玫瑰团长.dds",talk_title = 20161111,talk_text = 3736302,}
event_hint[3736401] ={task_id = 37364,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3736401,}
event_hint[3736601] ={task_id = 37366,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3736601,next_hint = 3736602,}
event_hint[3736602] ={talk_npc_icon = "npc头像\\首席政务执行官.dds",talk_title = 20161124,talk_text = 3736602,next_hint = 3736603,}
event_hint[3736603] ={talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3736603,next_hint = 3736604,}
event_hint[3736604] ={talk_npc_icon = "npc头像\\首席政务执行官.dds",talk_title = 20161124,talk_text = 3736604,next_hint = 3736605,}
event_hint[3736605] ={talk_npc_icon = "npc头像\\首席政务执行官.dds",talk_title = 20161124,talk_text = 3736605,next_hint = 3736606,}
event_hint[3736606] ={talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3736606,next_hint = 3736607,}
event_hint[3736607] ={talk_npc_icon = "npc头像\\首席政务执行官.dds",talk_title = 20161124,talk_text = 3736607,}
event_hint[3788601] ={task_id = 37886,gfx_path = "策划联入\\状态效果\\界面_雨水.gfx",gfx_pos_x = 512,gfx_pos_y = 384,talk_npc_icon = "npc头像\\白帝城平民01.dds",talk_title = 20161126,talk_text = 3788601,next_hint =3788602,}
event_hint[3788602] ={talk_npc_icon = "npc头像\\白帝城平民06.dds",talk_title = 20161125,talk_text = 3788602,}
event_hint[3766701] ={task_id = 37667,talk_npc_icon = "npc头像\\羽灵.dds",talk_title = 20161127,talk_text = 3766701,}
event_hint[3766801] ={task_id = 37668,talk_npc_icon = "npc头像\\羽灵.dds",talk_title = 20161127,talk_text = 3766801,}
event_hint[3797201] ={task_id = 37972,talk_npc_icon = "npc头像\\羽灵.dds",talk_title = 20161127,talk_text = 3797201,}
event_hint[3797301] ={task_id = 37973,talk_npc_icon = "npc头像\\男羽芒.dds",talk_title = 20161128,talk_text = 3797301,}
event_hint[3797401] ={task_id = 37974,talk_npc_icon = "npc头像\\男羽芒.dds",talk_title = 20161128,talk_text = 3797401,}
event_hint[3797501] ={task_id = 37975,talk_npc_icon = "npc头像\\男羽芒.dds",talk_title = 20161128,talk_text = 3797501,}
event_hint[3797601] ={task_id = 37976,talk_npc_icon = "npc头像\\武侠.dds",talk_title = 20161129,talk_text = 3797601,}
event_hint[3797701] ={task_id = 37977,talk_npc_icon = "npc头像\\武侠.dds",talk_title = 20161129,talk_text = 3797701,}
event_hint[3797801] ={task_id = 37978,talk_npc_icon = "npc头像\\武侠.dds",talk_title = 20161129,talk_text = 3797801,}
event_hint[3797901] ={task_id = 37979,talk_npc_icon = "npc头像\\妖精.dds",talk_title = 20161130,talk_text = 3797901,}
event_hint[3798001] ={task_id = 37980,talk_npc_icon = "npc头像\\妖精.dds",talk_title = 20161130,talk_text = 3798001,}
event_hint[3798101] ={task_id = 37981,talk_npc_icon = "npc头像\\妖精.dds",talk_title = 20161130,talk_text = 3798101,}
event_hint[3798201] ={task_id = 37982,talk_npc_icon = "npc头像\\妖兽.dds",talk_title = 20161131,talk_text = 3798201,}
event_hint[3798301] ={task_id = 37983,talk_npc_icon = "npc头像\\妖兽.dds",talk_title = 20161131,talk_text = 3798301,}
event_hint[3798401] ={task_id = 37984,talk_npc_icon = "npc头像\\妖兽.dds",talk_title = 20161131,talk_text = 3798401,}
event_hint[3776201] ={task_id = 37762,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20161122,talk_text = 3776201,}
event_hint[3736501] ={task_id = 37365,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3736501,}
event_hint[3788801] ={task_id = 37888,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3788801,}
event_hint[3738901] ={task_id = 37389,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3738901,}
event_hint[3738401] ={task_id = 37384,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3738401,}
event_hint[3799701] ={task_id = 37997,talk_npc_icon = "npc头像\\年轻的亚力山大.dds",talk_title = 20161132,talk_text = 3799701,next_hint = 3799702,}
event_hint[3799702] ={talk_npc_icon = "npc头像\\年轻的古德希尔.dds",talk_title = 20161133,talk_text = 3799702,next_hint = 3799703,}
event_hint[3799703] ={talk_npc_icon = "npc头像\\年轻的亚力山大.dds",talk_title = 20161132,talk_text = 3799703,next_hint = 3799704,}
event_hint[3799704] ={talk_npc_icon = "npc头像\\年轻的古德希尔.dds",talk_title = 20161133,talk_text = 3799704,next_hint = 3799705,}
event_hint[3799705] ={talk_npc_icon = "npc头像\\年轻的亚力山大.dds",talk_title = 20161132,talk_text = 3799705,next_hint = 3799706,}
event_hint[3799706] ={talk_npc_icon = "npc头像\\年轻的古德希尔.dds",talk_title = 20161133,talk_text = 3799706,}
event_hint[3805301] ={task_id = 38053,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3805301,next_hint = 3805302,}
event_hint[3805302] ={talk_npc_icon = "npc头像\\夏风将军.dds",talk_title = 20161108,talk_text = 3805302,next_hint = 3805303,}
event_hint[3805303] ={talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3805303,next_hint = 3805304,}
event_hint[3805304] ={talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3805304,next_hint = 3805305,}
event_hint[3805305] ={talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3805305,next_hint = 3805306,}
event_hint[3805306] ={talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3805306,}
event_hint[3805701] ={task_id = 38057,talk_npc_icon = "npc头像\\玫瑰团长.dds",talk_title = 20161111,talk_text = 3805701,next_hint = 3805702,}
event_hint[3805702] ={talk_npc_icon = "npc头像\\玫瑰副团长.dds",talk_title = 20161112,talk_text = 3805702,next_hint = 3805703,}
event_hint[3805703] ={talk_npc_icon = "npc头像\\玫瑰团长.dds",talk_title = 20161111,talk_text = 3805703,next_hint = 3805704,}
event_hint[3805704] ={talk_npc_icon = "npc头像\\玫瑰副团长.dds",talk_title = 20161112,talk_text = 3805704,next_hint = 3805705,}
event_hint[3805705] ={talk_npc_icon = "npc头像\\玫瑰团长.dds",talk_title = 20161111,talk_text = 3805705,next_hint = 3805706,}
event_hint[3805706] ={talk_npc_icon = "npc头像\\玫瑰副团长.dds",talk_title = 20161112,talk_text = 3805706,next_hint = 3805707,}
event_hint[3805707] ={talk_npc_icon = "npc头像\\玫瑰团长.dds",talk_title = 20161111,talk_text = 3805707,next_hint = 3805708,}
event_hint[3805708] ={talk_npc_icon = "npc头像\\玫瑰副团长.dds",talk_title = 20161112,talk_text = 3805708,next_hint = 3805709,}
event_hint[3805709] ={talk_npc_icon = "npc头像\\玫瑰团长.dds",talk_title = 20161111,talk_text = 3805709,next_hint = 3805710,}
event_hint[3805710] ={talk_npc_icon = "npc头像\\玫瑰副团长.dds",talk_title = 20161112,talk_text = 3805710,}
event_hint[3886001] ={task_id = 38860,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3886001,}
event_hint[3886101] ={task_id = 38861,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3886101,}
event_hint[3886201] ={task_id = 38862,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3886201,}
event_hint[3886301] ={task_id = 38863,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3886301,}
event_hint[3885601] ={task_id = 38856,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3885601,}
event_hint[3885701] ={task_id = 38857,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3885701,}
event_hint[3885801] ={task_id = 38858,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3885801,}
event_hint[3885901] ={task_id = 38859,talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3885901,next_hint = 3885902,}
event_hint[3885902] ={talk_npc_icon = "npc头像\\岱卡法沙.dds",talk_title = 20170501,talk_text = 3885902,next_hint = 3885903,}
event_hint[3885903] ={talk_npc_icon = "npc头像\\白银席尔瓦.dds",talk_title = 20161119,talk_text = 3885903,next_hint = 3885904,}
event_hint[3885904] ={talk_npc_icon = "npc头像\\岱卡法沙.dds",talk_title = 20170501,talk_text = 3885904,}
event_hint[3874501] ={task_id = 38745,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3874501,}
event_hint[3874601] ={task_id = 38746,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3874601,}
event_hint[3874701] ={task_id = 38747,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3874701,}
event_hint[3874801] ={task_id = 38748,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3874801,}
event_hint[3874901] ={task_id = 38749,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3874901,}
event_hint[3875001] ={task_id = 38750,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3875001,}
event_hint[3314701] ={task_id = 33147,talk_npc_icon = "npc头像\\祖龙城长老.dds",talk_title = 20170503,talk_text = 3314701,}
event_hint[38567] ={task_id =38567,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851301,}
event_hint[38570] ={task_id =38570,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851301,}
event_hint[38573] ={task_id =38573,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851301,}
event_hint[38576] ={task_id =38576,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851401,}
event_hint[38579] ={task_id =38579,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851401,}
event_hint[38582] ={task_id =38582,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851401,}
event_hint[38585] ={task_id =38585,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851501,}
event_hint[38588] ={task_id =38588,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851501,}
event_hint[38591] ={task_id =38591,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851501,}
event_hint[38594] ={task_id =38594,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851601,}
event_hint[38597] ={task_id =38597,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851601,}
event_hint[38600] ={task_id =38600,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851601,}
event_hint[38603] ={task_id =38603,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851701,}
event_hint[38606] ={task_id =38606,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851701,}
event_hint[38609] ={task_id =38609,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851701,}
event_hint[38612] ={task_id =38612,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851701,}
event_hint[38615] ={task_id =38615,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851701,}
event_hint[38618] ={task_id =38618,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851801,}
event_hint[38621] ={task_id =38621,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851801,}
event_hint[38624] ={task_id =38624,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851801,}
event_hint[38627] ={task_id =38627,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851801,}
event_hint[38630] ={task_id =38630,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851801,}
event_hint[38633] ={task_id =38633,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851901,}
event_hint[38636] ={task_id =38636,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851901,}
event_hint[38639] ={task_id =38639,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851901,}
event_hint[38642] ={task_id =38642,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851901,}
event_hint[38645] ={task_id =38645,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3851901,}
event_hint[38648] ={task_id =38648,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852001,}
event_hint[38651] ={task_id =38651,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852001,}
event_hint[38654] ={task_id =38654,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852001,}
event_hint[38657] ={task_id =38657,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852001,}
event_hint[38660] ={task_id =38660,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852001,}
event_hint[38663] ={task_id =38663,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852101,}
event_hint[38666] ={task_id =38666,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852101,}
event_hint[38669] ={task_id =38669,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852101,}
event_hint[38672] ={task_id =38672,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852101,}
event_hint[38675] ={task_id =38675,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852101,}
event_hint[38678] ={task_id =38678,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852201,}
event_hint[38681] ={task_id =38681,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852201,}
event_hint[38684] ={task_id =38684,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852201,}
event_hint[38687] ={task_id =38687,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852201,}
event_hint[38690] ={task_id =38690,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170502,talk_text = 3852201,}
event_hint[3953101] ={task_id = 39531,talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170901,talk_text = 3953101,next_hint = 3953102,}
event_hint[3953102] ={talk_npc_icon = "npc头像\\万流长老.dds",talk_title = 20170901,talk_text = 3953102,next_hint = 3953103,}
event_hint[3953103] ={talk_npc_icon = "npc头像\\万流长老.dds",talk_title =20170901,talk_text = 3953103,}
event_hint[3953301] ={task_id = 39533,talk_npc_icon = "npc头像\\白帝城平民05.dds",talk_title = 20170902,talk_text = 3953301,next_hint = 3953302,}
event_hint[3953302] ={talk_npc_icon = "npc头像\\白帝城平民05.dds",talk_title = 20170902,talk_text = 3953302,}
event_hint[3972801] ={task_id = 39728,talk_npc_icon = "npc头像\\春节姜小虎.dds",talk_title = 20180101,talk_text = 3972801,}
event_hint[3972901] ={task_id = 39729,talk_npc_icon = "npc头像\\春节姜小虎.dds",talk_title = 20180101,talk_text = 3972901,}
event_hint[3973001] ={task_id = 39730,talk_npc_icon = "npc头像\\春节姜小虎.dds",talk_title = 20180101,talk_text = 3973001,}
event_hint[3973301] ={task_id = 39733,talk_npc_icon = "npc头像\\春节姜小虎.dds",talk_title = 20180101,talk_text = 3973301,}
event_hint[3973401] ={task_id = 39734,talk_npc_icon = "npc头像\\春节姜小虎.dds",talk_title = 20180101,talk_text = 3973401,}
event_hint[3973501] ={task_id = 39735,talk_npc_icon = "npc头像\\春节姜小虎.dds",talk_title = 20180101,talk_text = 3973501,}
event_hint[3974301] ={task_id = 39743,talk_npc_icon = "npc头像\\春节姜小虎.dds",talk_title = 20180101,talk_text = 3974301,}
event_hint[3974401] ={task_id = 39744,talk_npc_icon = "npc头像\\春节姜小虎.dds",talk_title = 20180101,talk_text = 3974401,}
event_hint[3977601] ={task_id = 39776,talk_npc_icon = "npc头像\\春节姜小虎.dds",talk_title = 20180101,talk_text = 3977601,}
event_hint[3977701] ={task_id = 39777,talk_npc_icon = "npc头像\\春节姜小虎.dds",talk_title = 20180101,talk_text = 3977701,}
event_hint[3994401] ={task_id = 39944,talk_npc_icon = "npc头像\\卖花女.dds",talk_title = 20180507,talk_text = 3994401,next_hint = 3994402,}
event_hint[3994402] ={talk_npc_icon = "npc头像\\白帝城平民01.dds",talk_title = 20180508,talk_text = 3994402,next_hint = 3994403,}
event_hint[3994403] ={talk_npc_icon = "npc头像\\卖花女.dds",talk_title = 20180507,talk_text = 3994403,next_hint = 3994404,}
event_hint[3994404] ={talk_npc_icon = "npc头像\\白帝城平民01.dds",talk_title = 20180508,talk_text = 3994404,}
event_hint[3990801] ={task_id = 39908,talk_npc_icon = "npc头像\\白帝城女法师.dds",talk_title = 20180509,talk_text = 3990801,}
event_hint[3992101] ={task_id = 39921,talk_npc_icon = "npc头像\\白帝城平民05.dds",talk_title = 20180510,talk_text = 3992101,next_hint = 3992102,}
event_hint[3992102] ={talk_npc_icon = "npc头像\\年轻的古德希尔.dds",talk_title = 20180511,talk_text = 3992102,next_hint = 3992103,}
event_hint[3992103] ={talk_npc_icon = "npc头像\\白帝城平民05.dds",talk_title = 20180510,talk_text = 3992103,next_hint = 3992104,}
event_hint[1873001] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873001,}
event_hint[1873002] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873002,}
event_hint[1873003] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873003,}
event_hint[1873004] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873004,}
event_hint[1873005] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873005,next_hint = 1873006 ,}
event_hint[1873006] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873006,}
event_hint[1873007] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873007,}
event_hint[1873008] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873008,next_hint = 1873009 ,}
event_hint[1873009] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873009,next_hint = 1873010 ,}
event_hint[1873010] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873010,}
event_hint[1873011] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873011,}
event_hint[1873012] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873012,}
event_hint[1873013] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873013,}
event_hint[1873014] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873014,next_hint = 1873015 ,}
event_hint[1873015] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873015,next_hint = 1873016 ,}
event_hint[1873016] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873016,}
event_hint[1873017] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873017,}
event_hint[1873018] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873018,}
event_hint[1873019] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873019,}
event_hint[1873020] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873020,}
event_hint[1873021] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873021,}
event_hint[1873022] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873022,}
event_hint[1873023] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873023,}
event_hint[1873024] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873024,next_hint = 1873025 ,}
event_hint[1873025] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873025,}
event_hint[1873026] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873026,}
event_hint[1873027] ={talk_npc_icon = "npc头像\\翼云扬.dds",talk_title =20180702 ,talk_text = 1873027,}
event_hint[1873028] ={talk_npc_icon = "npc头像\\云杏力.dds",talk_title =20180703 ,talk_text = 1873028,}
event_hint[1873029] ={talk_npc_icon = "npc头像\\羽遵.dds",talk_title =20180704 ,talk_text = 1873029,}
event_hint[1873030] ={talk_npc_icon = "npc头像\\翼南振.dds",talk_title =20180705 ,talk_text = 1873030,}
event_hint[1873031] ={talk_npc_icon = "npc头像\\重云01.dds",talk_title =20180706 ,talk_text = 1873031,}
event_hint[1873032] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873032,}
event_hint[1873098] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873098,}
event_hint[1873099] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873099,}
event_hint[1873101] ={talk_npc_icon = "npc头像\\明月姬01.dds",talk_title =20180701 ,talk_text = 1873101,}
event_hint[1000010] ={task_id = 40424,talk_npc_icon = "npc头像\\白帝城平民02.dds",talk_title = 20181009,talk_text = 1000010,next_hint = 1000011,}
event_hint[1000011] ={talk_npc_icon = "npc头像\\白帝城小怪04.dds",talk_title = 20181010,talk_text = 1000011,next_hint = 1000012,}
event_hint[1000012] ={talk_npc_icon = "npc头像\\白帝城平民02.dds",talk_title = 20181009,talk_text = 1000012,}
event_hint[1000013] ={task_id = 40492,talk_npc_icon = "npc头像\\沈寒初.dds",talk_title = 20181016,talk_text = 1000013,next_hint = 1000014,}
event_hint[1000014] ={talk_npc_icon = "npc头像\\小脚丫.dds",talk_title = 20181015,talk_text = 1000014,next_hint = 1000015,}
event_hint[1000015] ={talk_npc_icon = "npc头像\\沈寒初.dds",talk_title = 20181016,talk_text = 1000015,next_hint = 1000016,}
event_hint[1000016] ={talk_npc_icon = "npc头像\\小脚丫.dds",talk_title = 20181015,talk_text = 1000016,next_hint = 1000017,}
event_hint[1000017] ={talk_npc_icon = "npc头像\\沈寒初.dds",talk_title = 20181016,talk_text = 1000017,}
event_hint[1000018] ={task_id = 40500,talk_npc_icon = "npc头像\\沈寒初.dds",talk_title = 20181016,talk_text = 1000018,next_hint = 1000019,}
event_hint[1000019] ={talk_npc_icon = "npc头像\\李言蹊.dds",talk_title = 20181017,talk_text = 1000019,next_hint = 1000020,}
event_hint[1000020] ={talk_npc_icon = "npc头像\\沈寒初.dds",talk_title = 20181016,talk_text = 1000020,next_hint = 1000021,}
event_hint[1000021] ={talk_npc_icon = "npc头像\\李言蹊.dds",talk_title = 20181017,talk_text = 1000021,}
event_hint[3796101] ={task_id = 37961,gfx_path = "策划联入\\状态效果\\界面_萤火虫.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3796601] ={task_id = 37966,gfx_path = "策划联入\\状态效果\\界面_蝴蝶.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3778301] ={task_id = 37783,gfx_path = "策划联入\\状态效果\\界面_血.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3779001] ={task_id = 37790,gfx_path = "策划联入\\状态效果\\界面_血.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805401] ={task_id = 38054,gfx_path = "策划联入\\状态效果\\界面_礼花.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805402] ={task_id = 31697,gfx_path = "策划联入\\状态效果\\界面_蝴蝶.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805403] ={task_id = 31695,gfx_path = "策划联入\\状态效果\\界面_灵魂.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805404] ={task_id = 32440,gfx_path = "策划联入\\状态效果\\界面_礼花.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805405] ={task_id = 32441,gfx_path = "策划联入\\状态效果\\界面_礼花.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805406] ={task_id = 32442,gfx_path = "策划联入\\状态效果\\界面_礼花.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805407] ={task_id = 32443,gfx_path = "策划联入\\状态效果\\界面_礼花.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805408] ={task_id = 32444,gfx_path = "策划联入\\状态效果\\界面_礼花.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805409] ={task_id = 38943,gfx_path = "策划联入\\状态效果\\界面_礼花.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805410] ={task_id = 38967,gfx_path = "策划联入\\状态效果\\界面_蝴蝶.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805411] ={task_id = 39200,gfx_path = "策划联入\\状态效果\\界面_灵魂.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805412] ={task_id = 39632,gfx_path = "界面\\圣诞节掉礼物.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805413] ={task_id = 39633,gfx_path = "界面\\圣诞节掉礼物.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805414] ={task_id = 39634,gfx_path = "界面\\圣诞节掉礼物.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805415] ={task_id = 39627,gfx_path = "界面\\圣诞节掉礼物.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805416] ={task_id = 39851,gfx_path = "策划联入\\状态效果\\界面_蝴蝶.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805417] ={task_id = 39799,gfx_path = "界面\\愚界面.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805418] ={task_id = 39805,gfx_path = "界面\\愚界面.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805419] ={task_id = 39811,gfx_path = "界面\\愚界面.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805420] ={task_id = 40005,gfx_path = "策划联入\\状态效果\\界面_灵魂.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805422] ={task_id = 40062,gfx_path = "策划联入\\状态效果\\界面_蝴蝶.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805422] ={task_id = 39893,gfx_path = "界面\\雾散.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805422] ={task_id = 39873,gfx_path = "策划联入\\状态效果\\界面_萤火虫.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805421] ={task_id = 40331,talk_title = 20180720,talk_text = 1000001, interface_name="diary.xml"}
event_hint[3805422] ={task_id = 40332,talk_title = 20180720,talk_text = 1000002, interface_name="diary.xml"}
event_hint[3805423] ={task_id = 40333,talk_title = 20180720,talk_text = 1000003, interface_name="diary.xml"}
event_hint[3805424] ={task_id = 40334,talk_title = 20180720,talk_text = 1000004, interface_name="diary.xml"}
event_hint[3805425] ={task_id = 40335,talk_title = 20180724,talk_text = 1000005, interface_name="diary.xml"}
event_hint[3805426] ={task_id = 38972,gfx_path = "界面\\黑色羽毛.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805427] ={task_id = 38973,gfx_path = "界面\\模糊状态.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[3805428] ={task_id = 30546,talk_text = 1970748,interface_name="newguidecomplete.xml"}
event_hint[3805429] ={task_id = 30547,talk_text = 1970749,interface_name="newguidecomplete.xml"}
event_hint[3805430] ={task_id = 30548,talk_text = 1970750,interface_name="newguidecomplete.xml"}
event_hint[3805431] ={task_id = 30549,talk_text = 1970751,interface_name="newguidecomplete.xml"}
event_hint[3805432] ={task_id = 30550,talk_text = 1970752,interface_name="newguidecomplete.xml"}
event_hint[3805433] ={task_id = 30551,talk_text = 1970753,interface_name="newguidecomplete.xml"}
event_hint[3805434] ={task_id = 30552,talk_text = 1970754,interface_name="newguidecomplete.xml"}
event_hint[3805435] ={task_id = 30553,talk_text = 1970755,interface_name="newguidecomplete.xml"}
event_hint[3805436] ={task_id = 30555,talk_text = 1970756,interface_name="newguidecomplete.xml"}
event_hint[3805437] ={task_id = 30556,talk_text = 1970757,interface_name="newguidecomplete.xml"}
event_hint[3805438] ={task_id = 30557,talk_text = 1970758,interface_name="newguidecomplete.xml"}
event_hint[3805439] ={task_id = 30558,talk_text = 1970759,interface_name="newguidecomplete.xml"}
event_hint[3805440] ={task_id = 30559,talk_text = 1970760,interface_name="newguidecomplete.xml"}
event_hint[3805441] ={task_id = 30560,talk_text = 1970761,interface_name="newguidecomplete.xml"}
event_hint[3805442] ={task_id = 30561,talk_text = 1970762,interface_name="newguidecomplete.xml"}
event_hint[3805443] ={task_id = 30562,talk_text = 1970763,interface_name="newguidecomplete.xml"}
event_hint[3805444] ={task_id = 30563,talk_text = 1970764,interface_name="newguidecomplete.xml"}
event_hint[3805445] ={task_id = 30564,talk_text = 1970765,interface_name="newguidecomplete.xml"}
event_hint[3805446] ={task_id = 30565,talk_text = 1970766,interface_name="newguidecomplete.xml"}
event_hint[3805447] ={task_id = 30566,talk_text = 1970767,interface_name="newguidecomplete.xml"}
event_hint[3805448] ={task_id = 30568,talk_text = 1970768,interface_name="newguidecomplete.xml"}
event_hint[3805449] ={task_id = 30569,talk_text = 1970769,interface_name="newguidecomplete.xml"}
event_hint[3805450] ={task_id = 30570,talk_text = 1970770,interface_name="newguidecomplete.xml"}
event_hint[3805451] ={task_id = 30571,talk_text = 1970771,interface_name="newguidecomplete.xml"}
event_hint[3805452] ={task_id = 30572,talk_text = 1970772,interface_name="newguidecomplete.xml"}
event_hint[3805453] ={task_id = 30573,talk_text = 1970773,interface_name="newguidecomplete.xml"}
event_hint[3805454] ={task_id = 31574,talk_text = 1970774,interface_name="newguidecomplete.xml"}
event_hint[3805455] ={task_id = 31575,talk_text = 1970775,interface_name="newguidecomplete.xml"}
event_hint[3805456] ={task_id = 33657,talk_text = 1970776,interface_name="newguidecomplete.xml"}
event_hint[3805457] ={task_id = 33655,talk_text = 1970777,interface_name="newguidecomplete.xml"}
event_hint[3805458] ={task_id = 33656,talk_text = 1970778,interface_name="newguidecomplete.xml"}
event_hint[3805459] ={task_id = 36345,talk_text = 1970779,interface_name="newguidecomplete.xml"}
event_hint[3805460] ={task_id = 36346,talk_text = 1970780,interface_name="newguidecomplete.xml"}
event_hint[3805461] ={task_id = 40097,talk_text = 1970781,interface_name="newguidecomplete.xml"}
event_hint[3805462] ={task_id = 40098,talk_text = 1970782,interface_name="newguidecomplete.xml"}
event_hint[3805463] ={task_id = 30567,talk_text = 1970783,interface_name="newguidecomplete.xml"}
event_hint[3805464] ={task_id = 35624,talk_text = 1970784,interface_name="newguidecomplete.xml"}
event_hint[3805465] ={task_id = 35625,talk_text = 1970785,interface_name="newguidecomplete.xml"}
event_hint[3805466] ={task_id = 35626,talk_text = 1970786,interface_name="newguidecomplete.xml"}
event_hint[3805467] ={task_id = 35627,talk_text = 1970787,interface_name="newguidecomplete.xml"}
event_hint[3805468] ={task_id = 35628,talk_text = 1970788,interface_name="newguidecomplete.xml"}
event_hint[3805469] ={task_id = 35629,talk_text = 1970789,interface_name="newguidecomplete.xml"}
event_hint[3805470] ={task_id = 35630,talk_text = 1970790,interface_name="newguidecomplete.xml"}
event_hint[3805471] ={task_id = 35631,talk_text = 1970791,interface_name="newguidecomplete.xml"}
event_hint[3805472] ={task_id = 35632,talk_text = 1970792,interface_name="newguidecomplete.xml"}
event_hint[3805473] ={task_id = 35633,talk_text = 1970793,interface_name="newguidecomplete.xml"}
event_hint[3805474] ={task_id = 35634,talk_text = 1970794,interface_name="newguidecomplete.xml"}
event_hint[3805475] ={task_id = 35635,talk_text = 1970795,interface_name="newguidecomplete.xml"}
event_hint[3805476] ={task_id = 35636,talk_text = 1970796,interface_name="newguidecomplete.xml"}
event_hint[3805477] ={task_id = 35637,talk_text = 1970797,interface_name="newguidecomplete.xml"}
event_hint[3805478] ={task_id = 35638,talk_text = 1970798,interface_name="newguidecomplete.xml"}
event_hint[3805479] ={task_id_space=1,task_id = 1,talk_text = 1970799,interface_name="newguidecomplete.xml"}
event_hint[3805480] ={task_id_space=1,task_id = 2,talk_text = 1970800,interface_name="newguidecomplete.xml"}
event_hint[3805481] ={task_id_space=1,task_id = 3,talk_text = 1970801,interface_name="newguidecomplete.xml"}
event_hint[3805482] ={task_id_space=1,task_id = 4,talk_text = 1970802,interface_name="newguidecomplete.xml"}
event_hint[3805483] ={task_id_space=1,task_id = 5,talk_text = 1970803,interface_name="newguidecomplete.xml"}
event_hint[3805484] ={task_id_space=1,task_id = 6,talk_text = 1970804,interface_name="newguidecomplete.xml"}
event_hint[3805485] ={task_id_space=1,task_id = 7,talk_text = 1970805,interface_name="newguidecomplete.xml"}
event_hint[3805486] ={task_id_space=1,task_id = 8,talk_text = 1970806,interface_name="newguidecomplete.xml"}
event_hint[3805487] ={task_id_space=1,task_id = 9,talk_text = 1970807,interface_name="newguidecomplete.xml"}
event_hint[3805488] ={task_id_space=1,task_id = 10,talk_text = 1970808,interface_name="newguidecomplete.xml"}
event_hint[3805489] ={task_id_space=1,task_id = 11,talk_text = 1970809,interface_name="newguidecomplete.xml"}
event_hint[3805490] ={task_id_space=1,task_id = 12,talk_text = 1970810,interface_name="newguidecomplete.xml"}
event_hint[3805491] ={task_id_space=1,task_id = 13,talk_text = 1970811,interface_name="newguidecomplete.xml"}
event_hint[3805492] ={task_id_space=1,task_id = 14,talk_text = 1970812,interface_name="newguidecomplete.xml"}
event_hint[3805493] ={task_id_space=1,task_id = 15,talk_text = 1970813,interface_name="newguidecomplete.xml"}
event_hint[3805494] ={task_id_space=1,task_id = 16,talk_text = 1970814,interface_name="newguidecomplete.xml"}
event_hint[3805495] ={task_id_space=1,task_id = 17,talk_text = 1970815,interface_name="newguidecomplete.xml"}
event_hint[3805496] ={task_id_space=1,task_id = 18,talk_text = 1970816,interface_name="newguidecomplete.xml"}
event_hint[3805497] ={task_id_space=1,task_id = 19,talk_text = 1970817,interface_name="newguidecomplete.xml"}
event_hint[3805498] ={task_id_space=1,task_id = 20,talk_text = 1970818,interface_name="newguidecomplete.xml"}
event_hint[3805499] ={task_id_space=1,task_id = 21,talk_text = 1970819,interface_name="newguidecomplete.xml"}
event_hint[3805500] ={task_id_space=1,task_id = 22,talk_text = 1970820,interface_name="newguidecomplete.xml"}
event_hint[3805501] ={task_id_space=1,task_id = 23,talk_text = 1970821,interface_name="newguidecomplete.xml"}
event_hint[3805502] ={task_id_space=1,task_id = 24,talk_text = 1970822,interface_name="newguidecomplete.xml"}
event_hint[3805503] ={task_id_space=1,task_id = 25,talk_text = 1970823,interface_name="newguidecomplete.xml"}
event_hint[3805504] ={task_id_space=1,task_id = 26,talk_text = 1970824,interface_name="newguidecomplete.xml"}
event_hint[3805505] ={task_id_space=1,task_id = 27,talk_text = 1970825,interface_name="newguidecomplete.xml"}
event_hint[3805506] ={task_id_space=1,task_id = 28,talk_text = 1970826,interface_name="newguidecomplete.xml"}
event_hint[3805507] ={task_id_space=1,task_id = 29,talk_text = 1970827,interface_name="newguidecomplete.xml"}
event_hint[3805508] ={task_id_space=1,task_id = 30,talk_text = 1970828,interface_name="newguidecomplete.xml"}
event_hint[3805509] ={task_id_space=1,task_id = 31,talk_text = 1970829,interface_name="newguidecomplete.xml"}
event_hint[3805510] ={task_id_space=1,task_id = 32,talk_text = 1970830,interface_name="newguidecomplete.xml"}
event_hint[3805511] ={task_id_space=1,task_id = 33,talk_text = 1970831,interface_name="newguidecomplete.xml"}
event_hint[3805512] ={task_id_space=1,task_id = 34,talk_text = 1970832,interface_name="newguidecomplete.xml"}
event_hint[3805513] ={task_id_space=1,task_id = 35,talk_text = 1970833,interface_name="newguidecomplete.xml"}
event_hint[3805514] ={task_id_space=1,task_id = 36,talk_text = 1970834,interface_name="newguidecomplete.xml"}
event_hint[3805515] ={task_id_space=1,task_id = 37,talk_text = 1970835,interface_name="newguidecomplete.xml"}
event_hint[3805516] ={task_id_space=1,task_id = 38,talk_text = 1970836,interface_name="newguidecomplete.xml"}
event_hint[3805517] ={task_id_space=1,task_id = 39,talk_text = 1970837,interface_name="newguidecomplete.xml"}
event_hint[3805518] ={task_id_space=1,task_id = 40,talk_text = 1970838,interface_name="newguidecomplete.xml"}
event_hint[3805519] ={task_id_space=1,task_id = 41,talk_text = 1970839,interface_name="newguidecomplete.xml"}
event_hint[3805520] ={task_id_space=1,task_id = 42,talk_text = 1970840,interface_name="newguidecomplete.xml"}
event_hint[3805521] ={task_id_space=1,task_id = 43,talk_text = 1970841,interface_name="newguidecomplete.xml"}
event_hint[3805522] ={task_id_space=1,task_id = 44,talk_text = 1970842,interface_name="newguidecomplete.xml"}
event_hint[3805523] ={task_id_space=1,task_id = 45,talk_text = 1970843,interface_name="newguidecomplete.xml"}
event_hint[3805524] ={task_id_space=1,task_id = 46,talk_text = 1970844,interface_name="newguidecomplete.xml"}
event_hint[3805525] ={task_id_space=1,task_id = 47,talk_text = 1970845,interface_name="newguidecomplete.xml"}
event_hint[3805526] ={task_id_space=1,task_id = 48,talk_text = 1970846,interface_name="newguidecomplete.xml"}
event_hint[3805527] ={task_id_space=1,task_id = 49,talk_text = 1970847,interface_name="newguidecomplete.xml"}
event_hint[3805528] ={task_id_space=1,task_id = 50,talk_text = 1970848,interface_name="newguidecomplete.xml"}
event_hint[3805529] ={task_id_space=1,task_id = 51,talk_text = 1970849,interface_name="newguidecomplete.xml"}
event_hint[3805530] ={task_id = 40429,gfx_path = "界面\\出师特效.gfx",gfx_pos_x = 512,gfx_pos_y = 384,}
event_hint[1970701] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970701,}
event_hint[1970702] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970702,}
event_hint[1970703] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970703,}
event_hint[1970704] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970704,}
event_hint[1970705] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970705,}
event_hint[1970706] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970706,}
event_hint[1970707] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970707,}
event_hint[1970708] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970708,}
event_hint[1970709] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970709,}
event_hint[1970710] ={talk_npc_icon = "npc头像\\云杏力01.dds",talk_title =20180703,talk_text = 1970710,}
event_hint[1970711] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970711,}
event_hint[1970712] ={talk_npc_icon = "npc头像\\云杏力01.dds",talk_title =20180703 ,talk_text = 1970712,}
event_hint[1970713] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970713,}
event_hint[1970714] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970714,}
event_hint[1970715] ={talk_npc_icon = "npc头像\\云杏力01.dds",talk_title =20180703 ,talk_text = 1970715,}
event_hint[1970716] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text =1970716,}
event_hint[1970717] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text =1970717,}
event_hint[1970718] ={talk_npc_icon = "npc头像\\云杏力01.dds",talk_title =20180703 ,talk_text = 1970718,}
event_hint[1970719] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970719,}
event_hint[1970720] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970720,}
event_hint[1970721] ={talk_npc_icon = "npc头像\\云杏力01.dds",talk_title =20180703 ,talk_text = 1970721,}
event_hint[1970722] ={talk_npc_icon = "npc头像\\云杏力01.dds",talk_title =20180703 ,talk_text = 1970722,}
event_hint[1970723] ={talk_npc_icon = "npc头像\\云杏力01.dds",talk_title =20180703 ,talk_text = 1970723,}
event_hint[1970724] ={talk_npc_icon = "npc头像\\黑明月姬01.dds",talk_title =20180701 ,talk_text = 1970724,}
event_hint[1970725] ={talk_npc_icon = "npc头像\\黑明月姬01.dds",talk_title =20180701 ,talk_text = 1970725,}
event_hint[1970726] ={talk_npc_icon = "npc头像\\黑重云.dds",talk_title =20180706 ,talk_text = 1970726,}
event_hint[1970727] ={talk_npc_icon = "npc头像\\黑明月姬01.dds",talk_title =20180701 ,talk_text = 1970727,}
event_hint[1970728] ={talk_npc_icon = "npc头像\\黑重云.dds",talk_title =20180706 ,talk_text = 1970728,}
event_hint[1970729] ={talk_npc_icon = "npc头像\\黑重云.dds",talk_title =20180706 ,talk_text = 1970729,}
event_hint[1970730] ={talk_npc_icon = "npc头像\\黑明月姬01.dds",talk_title =20180701 ,talk_text = 1970730,}
event_hint[1970731] ={talk_npc_icon = "npc头像\\黑重云.dds",talk_title =20180706 ,talk_text = 1970731,}
event_hint[1970732] ={talk_npc_icon = "npc头像\\黑明月姬01.dds",talk_title =20180701 ,talk_text = 1970732,}
event_hint[1970733] ={talk_npc_icon = "npc头像\\黑明月姬02.dds",talk_title =20180701 ,talk_text = 1970733,}
event_hint[1970734] ={talk_npc_icon = "npc头像\\黑重云.dds",talk_title =20180706 ,talk_text = 1970734,}
event_hint[1970735] ={talk_npc_icon = "npc头像\\黑明月姬02.dds",talk_title =20180701 ,talk_text = 1970735,}
event_hint[1970736] ={talk_npc_icon = "npc头像\\云杏力01.dds",talk_title =20180703 ,talk_text = 1970736,}
event_hint[1970737] ={talk_npc_icon = "npc头像\\黑明月姬02.dds",talk_title =20180701 ,talk_text = 1970737,}
event_hint[1970738] ={talk_npc_icon = "npc头像\\黑重云.dds",talk_title =20180706 ,talk_text = 1970738,}
event_hint[1970739] ={talk_npc_icon = "npc头像\\黑重云.dds",talk_title =20180706 ,talk_text = 1970739,}
event_hint[1970740] ={talk_npc_icon = "npc头像\\黑明月姬01.dds",talk_title =20180701 ,talk_text = 1970740,}
event_hint[1970741] ={talk_npc_icon = "npc头像\\黑重云.dds",talk_title =20180706 ,talk_text = 1970741,}
event_hint[1970742] ={talk_npc_icon = "npc头像\\黑重云.dds",talk_title =20180706 ,talk_text = 1970742,}
event_hint[1970743] ={talk_npc_icon = "npc头像\\紫淳.dds",talk_title =19799001 ,talk_text = 1970743,}
event_hint[1970744] ={talk_npc_icon = "npc头像\\云杏力01.dds",talk_title =20180703 ,talk_text = 1970744,}
event_hint[1970745] ={talk_npc_icon = "npc头像\\黑明月姬01.dds",talk_title =20180701 ,talk_text = 1970745,}
event_hint[1970746] ={talk_npc_icon = "npc头像\\黑明月姬02.dds",talk_title =20180701 ,talk_text = 1970746,}
event_hint[1970747] ={talk_npc_icon = "npc头像\\云杏力01.dds",talk_title =20180703 ,talk_text = 1970747,}