--[[
命令号：
	MMT_NONE = 0,
	MMT_COMMAND,								//	界面上任何COMMAND被触发
	MMT_TASK_COMPLETE,							//	任务完成
	MMT_REARRANGE_WINDOWS,						//	重新排列游戏界面
	MMT_ASTROLABE_EQUIPPED,						//	当前装备的星盘有变化（被撤下、或被新的替换）

	MMT_ASTROLABE_OPT_RESULT = 5,					//	星盘操作结果
	MMT_ASTROLABE_CLICK_SHOW_RAND_ADDON,		//	显示星盘属性随机界面
	MMT_ASTROLABE_RAND_ADDON_UPDATED,			//	星盘属性随机操作带来相关属性更新
	MMT_ASTROLABE_CLICK_SWALLOW,				//	点击星盘吞噬
	MMT_ASTROLABE_BEGIN_SWALLOW,				//	星盘吞噬已开启
	
	MMT_ASTROLABE_END_SWALLOW 			=10,	//	星盘吞噬已结束
	MMT_ASTROLABE_SWALLOW_UPDATED,				//	吞噬操作带来相关属性更新
	MMT_ASTROLABE_STOP_SWALLOW,					//	停止吞噬操作
	MMT_DIALOG_SHOWN,							//	对话框已显示（将触发对话框显示相关依赖逻辑）
	MMT_DIALOG_HIDDEN,							//	对话框已关闭（将触发对话框显示相关依赖逻辑）
	
	MMT_ASTROLABE_STOP_RANDADDON = 15,				//	停止随机星盘属性操作
	MMT_ASTROLABE_CLICK_SHOW_INC_POINTVALUE,	//	显示星盘涨资质界面
	MMT_ASTROLABE_STOP_INC_POINTVALUE,			//	停止星盘涨资质操作
	MMT_ASTROLABE_INC_POINTVALUE_UPDATED,		//	星盘涨资质操作带来相关属性更新
	MMT_ASTROLABE_BEGIN_INC_POINTVALUE,			//	星盘涨资质已开始
	
	MMT_ASTROLABE_END_INC_POINTVALUE = 20,			//	星盘涨资质已结束
	MMTQ_ASTROLABE_IS_SWALLOWING,				//	查询当前是否在星盘吞噬
	MMTQ_ASTROLABE_IS_RANDADDON,				//	是否正在随机星盘属性
	MMTQ_ASTROLABE_IS_INC_POINTVALUE,			//	是否正在涨资质操作
	MMT_SOLO_TOWER_CHALLENGE_OPT_RESULT,		//	单人爬塔副本操作结果
	
	MMT_SOLO_TOWER_CHALLENGE_SHOW_SELECTLAYER = 25,	//	单人爬塔副本显示选关界面
	MMT_SOLO_TOWER_CHALLENGE_HIDE_SELECTLAYER,	//	单人爬塔副本关闭选关界面
	MMT_SOLO_TOWER_CHALLENGE_CONFIRM_SELECTLAYER,	//	单人爬塔副本确认进行选关
	MMTQ_SOLO_TOWER_CHALLENGE_IS_SELECTLAYER_SHOWN,	//	单人爬塔副本选关界面是否显示
	MMT_SOLO_TOWER_CHALLENGE_TOWER_TASK_CHANGED,	//	单人爬塔副本爬塔任务变化
	
	MMT_SOLO_TOWER_CHALLENGE_VIEW_AWARD = 30,			//	单人爬塔副本查看奖励界面
	MMTQ_SOLO_TOWER_CHALLENGE_HAS_SELECTLAYER_NPC,	//	单人爬塔副本判断是否有选关NPC
	MMT_SOLO_TOWER_CHALLENGE_LOCAL_RANK_REFRESH,	//	单人爬塔本服排行榜刷新
	MMT_SOLO_TOWER_CHALLENGE_GLOBAL_RANK_REFRESH,	//	单人爬塔跨服排行榜刷新
	MMT_SOLO_TOWER_CHALLENGE_LOCAL_RANK_OPEN,		//	单人爬塔排行榜(本服)界面打开
	
	MMT_SOLO_TOWER_CHALLENGE_GLOBAL_RANK_OPEN = 35,		//	单人爬塔排行榜(跨服)界面打开
	MMT_SOLO_TOWER_CHALLENGE_CONFIRM_OPEN,			//	单人爬塔确认界面打开
	MMT_SOLO_TOWER_CHALLENGE_SHOW_SCORE_COST_DLG,	//	单人爬塔积分兑换界面打开
	MMT_SOLO_TOWER_CHALLENGE_HIDE_SCORE_COST_DLG,	//	单人爬塔积分兑换界面关闭
	MMT_SOLO_TOWER_CHALLENGE_SCORE_COST_SET_POS,	//	单人爬塔节分兑换界面设置位置
	
	MMT_PLAYER_FIGHT_SCORE_SHOW_WITH_POS = 40,			//	在特定位置显示人物战斗力界面
	MMT_MNFACTION_WORLD_STATE_UPDATE,			//	跨服帮战世界信息更新
	MMT_MNFACTION_WAR_APPLY_SHOW,				//	跨服帮战报名界面打开
	MMT_MNFACTION_WAR_APPLY_RESULT_HINT,			//	跨服帮战报名结果提示
	MMT_MNFACTION_WAR_APPLY_RESULT_SHOW,			//	跨服帮战报名结果界面显示
	
	MMT_MNFACTION_WORDLMAP_SHOW = 45,				//	打开跨服战世界地图
	MMT_MNFACTION_WORDLMAP_HIDE,				//	关闭跨服战世界地图
	MMTQ_MNFACTION_WORDLMAP_IS_SHOW,			//	跨服战世界地图是否显示
	MMT_MNFACTION_WORLDMAPDETAIL_SHOW_WITH_POS,     //  在特定位置显示帮战地图界面
	MMT_MNFACTION_PVPBATTLECHOOSE_SHOW,             //  帮战地图选择显示界面 
	
	MMT_NMFACTION_BATTLEFIELD_INFO_SHOW = 50,		//	跨服帮战战场信息界面打开
	MMT_MNFACTION_TRANSMIT_SHOW,				//	跨服帮战战场传送界面打开
	MMT_MNFACTION_RANK_LIST_SHOW,				//	跨服帮战排行榜界面打开
	MMT_MNFACTION_RANK_LIST_REFRESH,			//	跨服帮战排行榜刷新
	MMT_MNFACTION_BATTLE_RESULT_UPDATE,			//	跨服帮战战斗结果更新
	
	MMT_MNFACTION_BATTLEFIELD_MAP_SWITCH_SHOW =55,	//	跨服帮战战场地图显示/隐藏切换
	MMT_MNFACTION_BATTLEFIELD_MAP_UPDATE_HINT,	//	跨服帮战战场地图更新Hint
	MMT_MNFACTION_HELP_SHOW,	//	跨服帮战帮助界面打开
	MMT_MNFACTION_HELP_HIDE,	//	跨服帮战帮助界面关闭
	MMT_CROSS_SERVER_CRY_ADD_MSG,		//	增加跨服喊话滚动文字
	
	MMT_FIREWORKS2_VIEW_POP = 60,	//	弹出新烟花界面
	MMT_FIREWORKS2_SET_ITEM,	//	设置新烟花界面的烟花物品
	MMT_FIX_POS_TRANSMIT_POP,	//	弹出定点传送界面
	MMT_FIX_POS_TRANSMIT_UPDATE_POS,	//	更新定点传送界面传送点
	MMT_FIX_POS_TRANSMIT_UPDATE_ENERGY,	//	更新定点传送界面能量

	MMT_ENEMYLIST_UPDATELIST = 65,  
    MMT_ENEMYEXLIST_INFO,
	MMT_VIP_HELP_POP,			//	弹出VIP帮助界面
	MMT_HOME_OBJ_OP_SHOW_AND_SET_POS,	// 显示/隐藏家园物品操作界面，并设置位置
	MMT_HOME_OBJ_OP_SET_DIR_DEG,		// 设置家园物品操作界面方向显示
	
	MMT_HOME_OBJ_OP_SET_NAME = 70,			// 设置家园物品操作界面上的物品名称
	MMT_HOME_MAIN_INFO_UPDATE,          //家园主页面更新
	MMT_HOME_MAIN_SETTING,              //setting界面
	MMT_HOME_OTHER_INFO_UPDATE,         //其他人家园信息更新
	MMT_HOMEAPPRECIATE_OPEN,            //打开图鉴
	
	MMT_HOMEAPPRECIATE_BUILD_LIST = 75,      //创建土建列表  
	MMT_HOMEMAINDIALOG_OPEN,            //打开家园界面
	MMT_HOMEMAIN_PRAISE_UPDATE,         //家园点赞
	MMT_HOMEWORKSHOP_OPEN,              //打开工坊
	MMT_HOMEWORKSHOP_ALL_DELEGATE_OPEN, //打开代工簿 
	
	MMT_HOMEWORKSHOP_PURCHASE_OPEN = 80,     //打开购置
	MMT_HOMEWORKSHOP_BUID_LIST,         //创建工坊列表
	MMT_HOMEWORKSHOP_LEVEL_UP,          //工坊升级,及信息更新
	MMT_HOMEWORKSHOP_PURCHASE_LEVEL_UP, //购置升级
	MMT_HOMEWORKSHOP_PURCHASE_BUID_LIST,//创建购置列表
	
	MMT_HOMEWORKSHOP_BUILD_OWN_DELEGATE_LIST = 85,  //创建自己发布的代工信息
	MMT_HOMEWORKSHOP_GET_DELEGATE_MONEY,    //收取代工费
	MMT_HOMEWORKSHOP_PRODUCE_SUCCESS,       //生产成功
	MMT_HOMEWORKSHOP_PURCHASE_SUCCESS,       //购置成功
	MMT_HOMEWORKSHOP_BUILD_OTHER_DELEGATE_LIST, //创建其他的代工信息
	
	MMT_HOMEWORKSHOP_ADD_OWN_DELEGATE_GOODS = 90,   //添加要代工的物品
	MMT_HOMEWORKSHOP_ADD_OWN_DELEGATE_GOODS_CONFIRM,  //添加要代工的物品
	MMT_HOMEWORKSHOP_CHANGE_OWN_DELEGATE_GOODS_CONFIRM, //打开自己要添加的代工信息到确认对话框中
	MMT_HOMEWORKSHOP_SORT_ALL_DELEGATE_LIST,  //创建所有人的代工信息
	MMT_HOMEWORKSHOP_CHOOSE_HOME_TALENT,      //选择专精
	
	MMT_HOMEWORKSHOP_STOP_PRODUCE_WRONG = 95,      //因为一些问题停止生产
	MMT_HOMEWORKSHOP_STOP_PURCHASE_WRONG,      //因为一些问题停止购置
	MMT_HOMETASKDIALOG_SHOW_HIDE,			//打开关闭家园任务界面
	MMT_HOMETASKDIALOG_UPDATE,				//家园任务界面更新
	MMT_HOME_FARM_OPEN,						//打开农场，牧场界面
	
	MMT_HOME_FARM_INFO_UPDATE = 100,				//农场牧场信息更新
	MMT_HOME_FARM_PLANT_INFO_UPDATE,		//农场牧场种植信息更新
	MMT_HOME_PLANT_LIST_BUID,				//农场牧场种植选择
	MMT_HOME_RESOURCE_PRODUCE_INFO_OPEN,	//打开石木铁布资源生产界面
	MMT_HOME_RESOURCE_PRODUCE_INFO_UPDATE,	//石木铁布信息更新
	
	MMT_HOME_SWITCH_HOSTHEAD_AND_HOMEMAIN = 105,	//切换玩家头像和家园主界面
	MMT_HOME_KICKOUT_PLAYERS_SHOW,			//打开家园踢人界面
	MMT_HOME_KICKOUT_PLAYERS_REFRESH,		//刷新家园踢人界面
	MMT_HOME_HELP_OPEN,						//打开家园帮助界面
	MMT_HOME_EDIT_MODE_BEGIN,				//开启编辑模式
	
	MMT_SCRIPT_GFX_SHOW = 110,					//脚本在某个控件上放置提示光效
	MMT_HOMEEQUIPMENT_UPDATE,               //家园装备更新
	MMT_OPEN_PRAY_DIALOG,					//打开烧香祈福界面
	MMT_HOME_OP_HINT_SHOW_HIDE,				//家园操作提示界面打开关闭
	MMT_HOME_OP_HINT_SET_TEXT,				//家园操作提示界面设置文字
	
	MMT_HOMEAPPRECIATE_SHOW_WITH_ID = 115,        //通过家园物品ID打开图鉴
	MMT_HOME_BUILDING_HINT_SET_NAME,		//设置鼠标移到家园建筑时的名称提示
	MMT_HOME_ITEM_LINK_SHOW,				//打开家园物品链接
	MMT_HOMEWORKSHOP_DELEGATE_WITH_ID,      //通过图鉴物品ID打开代工簿
	MMT_OPEN_RED_PACKET_EDIT,				//打开红包编辑界面
	
	MMT_OPEN_RED_PACKET_RESULT = 120,				// 打开红包记录界面
	MMT_OPEN_RED_PACKET,					//打开红包领取界面
	MMT_UPDATE_RED_PACKET,					//更新红包领取界面
	MMT_UPDATE_RED_PACKET_RESULT,			//更新红包记录界面
	MMT_GET_RED_PACKET_INFO,				//向服务器请求红包信息

	MMT_PUSH_RED_PACKET_MSG_TO_FRIEND = 125,		//发送红包信息给好友，帮派，世界，
	MMT_HOME_RED_PACKAGE_FRLIST_OPEN,       //打开家园红包好友列表
	MMT_UPDATE_RED_PACKET_INFO,				//红包数据更新
	MMT_OPEN_RED_PACKET_MESSAGE,			//打开获得红包界面
	MMT_REQUEST_RED_PACKET_INFO,			//收到服务

	MMT_CHANGE_RED_PACKET_MODEL_STATE = 130,		//更改聚宝盆状态
	MMT_OPENG_RED_PACKET_GFX,				//打开红包特效界面
	MMT_POP_RANDOM_SOFT_KEYBOARD,			//打开随机软键盘
	MMT_OPEN_SELECT_TRANSMIT_MAP,			//打开 选择传送地图界面
	MMT_OPEN_TRANSMIT_MAPS,					//打开 传送的地图
	
	MMT_OPEN_TRANSMIT_MAPS_TOOL01 = 135,			//打开 传送的地图中的 控件界面（返回地图选择）
	MMT_OPEN_LOTTERY_MAIN,                  //打开彩票抽取界面
	MMT_LOTTERY_LAST_NOT_FINISH,            //上次抽奖还没完成
	MMT_LOTTERY_TEN_CARD_INFO_READY,        //随机抽取的10张卡牌数据已经准备好了
	MMT_LOTTERY_SOME_CARD_INFO_READY,       //抽取的某张卡牌数据已经准备好了
	
	MMT_LOTTERY_GET_CURREEN_SCORE = 140,          //获取当前的抽奖积分
	MMT_LOTTERY_FRESH_FLOOR_INFO,           //刷新楼层信息
	MMT_LOTTERY_FRESH_BOX_STORE_INFO,       //刷新彩票库存信息
	MMT_LOTTERY_BATCH_SETTING_DATA,         //彩票批量设置
	MMT_LOTTERY_SETTING_OPEN,               //打开批量设置

	MMT_LOTTERY_COST_HINT = 145,                   //花费消耗提示
	MMT_LOTTERY_SURE_OPEN_BOX,               //确实要花费开彩票
	MMT_LOTTERY_RESET_INTERFACE,             //如果界面卡死了则通知重直界面
	MMT_OPEN_GUARDIAN_BEAST_MAIN,			//打开守护兽主界面
	MMT_ON_SELECT_GUARDIAN_BEAST,			//点击主界面的守护兽

	MMT_REFRESH_GUARDIAN_BEAST_MAIN = 150,		//刷新守护兽主界面
	MMT_REFRESH_GUARDIAN_BEAST_HATCH,		//刷新守护兽蛋孵化界面
	MMT_OPEN_GUARDIAN_BEAST_MATCH,			//打开守护兽匹配
	MMT_GUARDIAN_BEAST_EGG_HATCH,			//守护兽蛋孵化
	MMT_NOTIFY_BEAST_BATTLE_INFO,			//通知计算守护兽战斗结果
	
	MMT_NOTIYF_REFRESH_DLG_REWARD = 155,			//刷新奖励界面
	MMT_OPEN_BEAST_BATTLE_REWARD,			//打开分数奖励界面
	MMT_NOTIFY_SELECTED_ENEMY_CHIEF,		//选择主将通知
	MMT_NOTIFY_BEAST_MAIN_PLAY_GFX,			//播放界面特效
	MMT_UPDATE_BEAST_RANK_LIST,				//更新守护兽排行榜
	
	MMT_SHOW_HOME_SKIN_DLG = 160,					//显示家园皮肤界面
	MMT_HOME_SKIN_SET_SUCCESS,				//更新家园皮肤界面
	MMT_HIDE_RESTORE_NORMAL_SKILL_BAR,		//隐藏/恢复普通技能栏
	MMTQ_NORMAL_SKILL_BAR_IS_SHOW,			//普通技能栏是否显示
	MMT_UPDATE_DYN_SKILL_BAR,				//更新动态技能栏
	
	MMT_HOME_CHANGE_RESOURCE_OPEN = 165,          //打开家园资源转换界面
	MMT_SPOUSE_ENTER_HOME,					//配偶进入家园
	MMT_HOME_BAG_OPEN,						//打开家园背包
	MMT_HOME_BAG_FRESH_INFO,				//更新家园背包信息		
	MMT_HANDBOOK_CHANGE_DES,                //改变图鉴皮肤描述
	
	MMT_PET_HANDBOOK_SELECTED = 170,				//图鉴中被选中的宠物
	MMT_SHOW_HOME_MIRROR_UPLOAD_DLG,		//打开家园上传界面
	MMT_UPDATE_HOME_MIRROR_UPLOAD_DLG,		//刷新家园上传界面
	MMT_SHOW_HOME_MIRROR_RANKLIST_DLG,		//打开家园镜像排行榜界面
	MMT_UPDATE_HOME_MIRROR_RANKLIST_DLG,	//刷新家园镜像排行榜界面
	
	MMT_NOTIFY_SELECT_EGG_COUNT = 175,			//通知选择了几个守护兽蛋
	MMT_REFRESH_BEAST_BAG,					//刷新守护兽背包
	MMT_OPEN_DLG_EASY_PRODUCE,				//打开快捷合成界面
	MMT_NOTIFY_EASY_PRODUCE,				//快速合成
	MMT_REFRESH_PACK_ITEM,					//背包物品变动

	MMT_ARENA_INPUT_TEAM_NAME = 180,				//打开竞技场组建战队输入名字界面
	MMT_ARENA_INVIATE_PLAYER,				//邀请队员
	MMT_ARENA_CREATE_TEAM_SUC,				//创建队伍成功
	MMT_ARENA_INVIATE_SOME_PLAYER_SUC,      //邀请某个队员成功
	MMT_ARENA_KICK_PLAYER_SUC,      		//踢掉某一个队员们成功
	
	MMT_ARENA_PLAYER_LEAVE_SUC = 185,				//某个队员离开成功
	MMT_ARENA_PLAYER_CAPTAIN_TRANS_SUC,		//转移队长成功
	MMT_SHOW_STATUE_PLAYER_PROPERTY,		//显示雕像属性
	MMT_SHOW_STATUE_ACTION_SELECT_DLG,		//打开雕像动作选择界面
	MMT_SHOW_HIDE_CARRIER_UP_DLG,			//显示隐藏上载具界面

	MMT_ARENA_CHOOSE_MATCH = 190,					//竞技场选择匹配方式
	MMT_ARENA_MATCHING,						//竞技场正在匹配
	MMT_ARENA_MATCH_SUCC,					//竞技场匹配成功
	MMT_ARENA_CAN_ENTER,					//可以进入竞技场
	MMT_ARENA_BATTLE_RESULT,                //竞技场战斗结果
	
	MMT_ARENA_BATTLE_COUNT_DOWN = 195,            //竞技场开始倒计时
	MMT_ARENA_BATTLE_GFX_COUNT_DOWM_OPEN,	//竞技场五秒倒计时
	MMT_ARENA_BATTLE_GFX_COUNT_DOWM_CLOSE,	//竞技场五秒倒计时
	MMT_ARENA_TEAM_RANK_LIST,				//竞技场队伍排行榜
	MMT_ARENA_PERSOON_RANK_LIST,			//竞技场个人排行榜
	
	MMT_ARENA_TEAM_RANK_LIST_REFRESH = 200,       //竞技场队伍排行榜刷新
	MMT_ARENA_TEAM_RANK_LIST_DETAIL_REFRESH, //竞技场排行榜队伍详细
	MMT_ARENA_PERSOON_RANK_LIST_REFRESH,     //竞技场个人排行榜刷新
	MMT_AREAN_OTHER_TEAM_INFO,               //打开查看某个人的竞技场
	MMT_ARENA_OTHER_TEAM_INFO_REFRESH,       //某个人竞技场刷新
	
	MMT_ARENA_BATTLE_RESULT_COUNT_DOWN = 205,      //竞技场结束倒计时
	MMT_ARENA_CAN_TAKE_PRICE,				 //竞技场可以领取代币
	MMT_SHOW_HIDE_CHRONICLE_DLG,            //显示隐藏编年史界面 
	MMT_ARENA_MY_FAKE_TEAM_OPEN,            //竞技场己方队伍
	MMT_ARENA_ENEMY_FAKE_TEAM_OPNE,			//竞技场敌方假队伍
	
	MMT_ARENA_REWARD_MONEY_SUC = 210,				//领取奖励成功
	MMT_ARENA_LEAVE_OPEN,					//离开竞技场界面
	MMT_OPEN_DLG_RENE,						//打开符文界面
	MMT_REFRESH_RUNE_SKILL_INFO,			//刷新技能符文信息
	MMT_NOTIFY_PLAY_RUNE_GFX,				//符文界面播放特效
    
	MMT_SHOW_HIDE_WORLD_MAP_DLG = 215,            //显示隐藏世界地图
	MMT_RESIZE_WORLD_MAP_DLG,               //适配三级世界地图窗口大小
	MMT_RESIZE_WORLD_MAP_IMAGE,             //适配三级地图背景 
	MMT_ARENA_BATTLE_RESULT_CAN_LEAVE,		//收到战斗结束之后可以离开
	MMT_ARENA_BATTLE_RESULT_CAN_NOT_LEAVE,  //战斗结束之后不能离开
	
	MMT_USE_CARRIER_SKILL = 220,					//使用载具技能
	MMT_ARENA_LAST_RECORD_OPEN,				//打开竞技场上次战斗的记录
	MMT_ARENA_ONLINE_INFO_UPDATA,			//竞技场在线信息更新
	MMT_GUILD_BATTLE_DECLARE_OPEN,			//打开宣战界面
	MMT_GUILD_BATTLE_DECLARE_FACTION,		//帮会信息已刷新
	
	MMT_GUILD_BATTLE_INVITE_OPEN = 225,			//打开联盟邀请界面
	MMT_GUILD_BATTLE_INVITE_HISTORY,		//打开邀请同盟历史界面
	MMT_GUILD_BATTLE_STATISTIC_OPEN,		//约战统计界面
	MMT_GUILD_BATTLE_DECLARE_SUC,			//我发起的约战成功
	MMT_GUILD_BATTLE_DECLARE_TO_ME,			//有人向我发起越战
	
	MMT_GUILD_BATTLE_DECLARE_AGREE = 230,			//我发起的约战被同意了
	MMT_GUILD_BATTLE_AGAINST_AGREE,			//别人发起的约战我同意了
	MMT_GUILD_BATTLE_DECLARE_REFUSE,		//我发起的约战被拒绝了
	MMT_GUILD_BATTLE_AGAINST_REFUSE,		//被人发起的约战被我拒绝了
	MMT_GUILD_BATTLE_DECLARE_END,			//我发起的约战战斗结束
	
	MMT_GUILD_BATTLE_AGAINST_END = 235,			//别人发起的约战战斗结束
	MMT_GUILD_BATTLE_ALLIACNE_INVITE_SENDER,	//我发送给别人的联盟邀请
	MMT_GUILD_BATTLE_ALLIACNE_INVITE_RECEIVER, //别人发送给我的联盟邀请
	MMT_GUILD_BATTLE_ALLIACNE_SENDER_AGREE,	//我邀请联盟被同意
	MMT_GUILD_BATTLE_ALLIACNE_RECEIVER_AGREE, //别人邀请我成为联盟被我同意
	
	MMT_GUILD_BATTLE_ALLIACNE_SENDER_REFUSE = 240,  //我邀请联盟被拒绝
	MMT_GUILD_BATTLE_ALLIACNE_RECEIVER_REFUSE, //别人邀请我成为联盟被我拒绝
	MMT_EMIT_BARRAGE,		// 发射弹幕
	MMT_OPEN_BARRAGESEND,		// 打开发射弹幕界面
	MMT_HIDE_BARRAGESEND,		// 关闭发射弹幕界面

	MMT_GUILD_BATTLE_ALLIANCE_OPEN = 245,				//打开联盟界面
	MMT_GUILD_BATTLE_HISTORY_OPEN,				//对战信息打开
	MMT_GUILD_BATTLE_OPEN,						//应战信息打开
	MMT_GUILD_BATTLE_STATISTIC_UPDATE,			//战斗统计信息更新
	MMT_GUILD_BATTLE_START,						//约战开始
	
	MMT_GUILD_BATTLE_END = 250,						//约战结束
	MMT_GUILD_BATTLE_WORD_OPEN,					//宣战，避战宣言
	MMT_OPEN_SIMULATOR_MAIN,					//打开装备模拟器主界面
	MMT_SIMULATOR_BACKTO_RIGHT_MAIN,			//装备模拟器右半部分回到角色页面
	MMT_SIMULATOR_UPDATE_DATA,					//更新界面
	
	MMT_SIMULATOR_SET_MID_DLG = 255,					//设置中间位置的dlg
	MMT_SIMULATOR_UPDATE_ASTROLABE,				//更新星盘模拟器
	MMT_LIBPRODUCE_MATERIAL_UPDATE,             //更新库对库生产原料
	MMT_LIBPRODUCE_PRODUCT_UPDATE,              //更新库对库生产成品 同时清空原料
	MMT_LIBPRODUCE_END_RPODUCE,                  //结束库对库生产生产

	MMT_LIBPRODUCE_ONE = 260,                         //库对库生产一个 清空材料包
	MMT_LIBPRODUCE_START_PRODUCE,
	MMT_PLAYER_VOTE_BEGIN,                     //玩家开始投票 
	MMT_PLAYER_VOTE_END,                       //玩家结束投票 
	MMT_OPEN_SIMULATOR_HELP,                   //打开模拟器帮助界面

	MMT_OPEN_DIVINATION = 265,						// 打开每日占卜界面
	MMT_OPEN_DIVINATION_DETAIL,					// 打开每日占卜详细信息界面
	MMT_REFRESH_DIVINATION,						// 刷新占卜
	MMT_OPEN_CAPTURE_DRIFT_BOTTLE,				// 打开捞漂流瓶界面
	MMT_OPEN_THROW_DRIFT_BOTTLE,				// 打开扔漂流瓶界面
	
	MMT_OPEN_ROLL_BALL_BOTTLE = 270,					// 打开漂流瓶滚动条界面
	MMT_OPEN_DRIFT_BOTTLE_MSG,					// 打开漂流瓶信息界面
	MMT_OPEN_DRIFT_BOTTLE_HISTORY,				// 打开扔漂流瓶历史记录
	MMT_FACTIONRECRUIT_UPDATERECRUIT,                  //要求更新帮派列表信息
	MMT_FACTIONRECRUIT_UPDATEAPPLY,
	
	MMT_FACTIONRECRUIT_APPLY = 275,				   //申请加入帮派
	MMT_FACTIONRECRUIT_SENDAPPLY,               // 发送申请请求
	MMT_FACTIONRECRUIT_RELEASE,                 //发送招募信息
	MMT_FACTIONRECRUIT_CANCEL,                  //取消招募信息
	MMT_FACTIONRECRUIT_REFUSE,					//拒绝某批人加入帮派
	
	MMT_FACTIONRECRUIT_ACCEPT = 280,					//同意某批人加入帮派
	MMT_FACTIONRECRUIT_APPLYREFRESH,            //刷新申请数据
	MMT_FACTIONRECRUIT_RECRUITFRESH,			//刷新招募数据
	MMT_FACTIONREECRUIT_OPEN,                    //刷新帮派招募界面
	MMT_FACTIONRECRUIT_PROTOCOL,                //处理帮派招募相关的服务器协议

	MMT_FACTIONSHARESETTING_ADDMANY = 285,            //批次加入列表
	MMT_FACTIONSHARESETTING_LISTADD,			//添加列表
	MMT_FACTIONSHARESETTING_LISTDEL,			//删除列表
	MMT_FACTIONSHARESETTING_LISTEDIT,			//编辑列表
	MMT_FACTIONSHARESETTING_LISTSAVE,           //保存列表
	
	MMT_FACTIONSHARESETTING_ADDNEWDIYLIST = 290,         //将选中的人存为自定义列表
	MMT_FACTIONSHARESETTING_CHANGEDIYNAME,      //修改自定义分组名称
	MMT_FACTIONSHARESETTING_SAVEDIYLIST,
	MMT_FACTIONSHARE_PAGECREATEOK,			 //完成创建分配页
	MMT_FACTIONSHARE_CREATEPAGE,			//创建分配页
	
	MMT_FACTIONSHARE_SHOWHISTOR = 295,			//展示分配记录
	MMT_FACTION_TALENT_TREE_OPEN,				//打开帮派天赋树
	MMT_FACTION_TALENT_TREE_CLOSE,				//关闭天赋树
	MMT_FACTION_REPARE_OPEN,					//帮派修理充值打开
	MMT_FACTION_BASE_INFO_UPDATE,				//帮派信息更新

	MMT_FACTION_TALENT_UPDATE = 300,					//帮派天赋更新
	MMT_FACTION_AUTO_CHECK_UPDATE,				//自动点天赋更新
	MMT_FACTION_INSTANCE_UPDATE,					//副本阶段打开
	MMT_FACTION_AWARD_SHOW_OPEN,						//帮派奖励
	MMT_FACTION_AWARD_UPDATE,					//帮派奖励更新
	
	MMT_FACTION_INSTANCE_CLOSE =305,					//副本阶段关闭
	MMT_FACTION_TARGET_OPEN,					//帮派目标
	MMT_FACTION_PET_SEVER,						//帮派宠物服务
	MMT_FACTION_TILL_SEVER,
	MMT_FACTION_PET_INFO,
	
	MMT_FACTION_TILL_INFO = 310,
	MMT_FACTION_PET_FEED,
	MMT_FACTION_PET_NESTING,
	MMT_FACTION_PET_CUTE,
	MMT_FACTION_TILL_HARVEST,
	
	MMT_FACTION_PET_FEED_ACTION=315,
	MMT_FACTION_PET_BLESS_ACTION,
	MMT_FACTION_TARGET_UPDATE,					//帮派目标更新
	MMT_PARK_RANK_LIST_OPEN,					//游乐场排行榜打开
	MMT_PARK_RANK_LIST_UPDATE,					//游乐场排行榜更新
	
	MMT_PARK_PENGPENG_RANK_OPEN = 320,				//碰碰车排行榜打开
	MMT_PARK_PENGPENG_RANK_UPDATE,				//碰碰车排行榜更新
	MMT_PLAYER_SITDOWN_NOTIFY,                      //帮派宠物暖床开始计时
	MMT_FACTION_STROEAGE_SERVER,            // 帮派仓库服务
	MMT_FACTION_ENABLE_EDITBUTTON,       //编辑按钮可用
	
	MMT_FACTION_QUERY_ITEM_IN_WHITE = 325,   // 判断物品是否在帮派白仓库名单
	MMT_ITEM_BUBBLE_CLOSE,
	MMT_FACTION_SCORE_BUBBLE_CLOSE,
	MMT_CROSS_BRIEF_INFO_CHANGED,       //收到新的跨服约战帮派列表数据
	MMT_CROSS_BRIEF_INFO_SEARCH,        //跨服约战宣战界面查询
	
	MMT_FACTION_UPDATE_RIGHTDOWN = 330,      //帮派权限下放
	MMT_FACTION_INSTANCE_RESET,
	MMT_FACTION_PET_ACTION,            //帮派宠物活动
	MMT_GUILD_BATTLE_DECLARE_REGISTED, //跨服约战报名
	MMT_GUILD_BATTLE_RESET_CROSSINFO,  //跨服帮战重新拉取信息
	
	MMT_GUILD_BATTLE_HISTORY_STATISTIC_UPDATE = 335, 
	MMT_GUILD_BATTLE_HISTORY_STATISTIC_OPEN,
	MMT_BOSSRUSH_AWARD_SHOW,
	MMT_BOSSRUSH_START_CLOCKING,
	MMT_BOSSRUSH_AWARD_UPDATE,
	
	MMT_OPEN_INSTANCE_MANUAL_MAIN = 340,			//副本手册
	MMT_REFRESH_MESSAGE_BOARD,				//副本手册留言板更新
	MMT_BIBLEREFINE_OPEN,		// 天书精炼界面
	MMT_BIBLEREFINETRANS_OPEN,	// 天书精炼转移
	MMT_BIBLEREFINE_START,		// 天书精炼开始，附带协议需要的信息

	MMT_BIBLEREFINE_END = 345,		// 天书精炼结束
	MMT_BIBLEREFINE_SENT,		// 发协议之后把天书index发给精炼界面，把进度条界面的位置发给成功/失败界面
	MMT_BIBLEREFINE_RESULT,		// 天书精炼结果
	MMT_BIBLEREFINE_TRANS_RES,	// 天书精炼转移结果
	MMT_ADVERTISEMENT_OPEN,//广告打开
	
	MMT_CHECK_ADVERTISEMENT_OPEN = 350, //查看广告是不是可以打开
	MMT_OPEN_MYSTERIOUS_SHOP,	// 打开神秘商店

	MMT_MAX,
--]]

--[[
wangzhihao 20180423
广告页面的显示

	show_advertisment = 0, // 是不是显示广告 0-不显示 1-显示
	advertisement_begin_time = "9999.02.01.12.23.00", //广告开始时间 年月日 时分秒 
	advertisement_end_time = "",//广告结束时间
	show_advertisment_pic = 1,//是不是显示广告顶部图片 0-不显示 1-显示
	advertisment_pic_url = "看看看", //广告顶部图片位置
	
	advertisement_type	0促销 1上新 2活动 3副本 4功能	
	
	advertisement_show = 0,   							--//是不是显示
	advertisement_type = 0,   							--//广告类型
	advertisement_name = "啦啦啦", 						--//广告的名字
	advertisement_begin_time = "2018.04.23.22.15.50",	--//开始时间
	advertisement_end_time = "2018.05.23.22.15.50", 	--//结束时间
	advertisement_pic_url = "测试",						--//广告类型图片
	advertisement_type_pic = 0，						--//广告类型图片 类型 0 正方形，1长方形
	advertisement_type_pic_url = "测试", 				--//广告类型底板图片路径
	advertisement_des1 = "我的天哪", 					--//大文字描述
	advertisement_des2 = "我的天哪开", 					--//小文字描述图片
    advertisement_goto_npc = 1, 						--//只要NPC ID >1 就回去寻找NPC
	advertisement_find_item = 1, 						--//只要ITEM ID > 0 就会打开商城
	advertisement_dialog_name = "",						--//打开某个界面的名字
	advertisement_dlg_msg_id = 0,                       --//打开界面的命令，对应上面的命令号
	advertisement_web_url = "www.baidu.com",            --//打开的网页 那个广告版新加一项
	player_ReincarnationCount = 0,                      --//转生次数
	player_level = 1,                                   --//等级
	player_RealmLevel = 0,                              --//境界等级
	["item_0"] = {    
        item_id = 6,
		item_count = 1,
    },
	["item_1"] = {    
        item_id = 5,
		item_count = 4,
    },
	
--]]

advertisement = {};

advertisement[1] =
{
	show_advertisment = 0,
	advertisement_begin_time = "2019.01.23.08.00.00",
	advertisement_end_time = "2019.02.12.24.00.00",
	show_advertisment_pic = 1,
	advertisment_pic_url = "version02\\系统\\推荐界面\\版头.tga",
	player_ReincarnationCount = 0,
	player_level = 1,
	player_RealmLevel = 0,
}

advertisement[2] =
{
	advertisement_show = 0,   							--//是不是显示
	advertisement_type = 2,   							--//广告类型
	advertisement_name = "问卷调研", 						--//广告的名字
	advertisement_begin_time = "2019.01.23.08.00.00",	--//开始时间
	advertisement_end_time = "2019.02.12.24.00.00", 	--//结束时间
	advertisement_pic_url = "version02\\系统\\推荐界面\\调研.tga",						--//广告类型图片
	advertisement_type_pic_url = "version02\\系统\\推荐界面\\丝带蓝色.tga", 				--//广告类型底板图片路径
	advertisement_type_pic = 0,						--//广告类型底板图片类型 0 正方形，1长方形
	advertisement_des1 = "2019.01.23-2019.02.12", 					--//大文字描述
	advertisement_des2 = "每个IP仅可完成一次,单账号下只有一个角色可获得奖励。\r请填写好期望领奖的账号、角色及服务器。\r完成问卷后7个工作日内将发放调研奖励。", 					--//小文字描述图片
	advertisement_goto_npc = 0, 						--//根据类型去往的NPC
	advertisement_find_item = 0, 						--//更具类型打开的商城物品
	advertisement_dialog_name = "",						--//找不到命令用dialog名字
	advertisement_web_url = "https://surveys.games.wanmei.com/survey/VBZ7Nv",            --//打开的网页 那个广告版新加一项	
	advertisement_dlg_msg_id = 0,                       --//打开界面的命令，
	player_ReincarnationCount = 0,
	player_level = 1,
	player_RealmLevel = 0,
	["item_0"] = {    
        item_id = 55583,
		item_count = 1,
	},
	["item_1"] = {    
        item_id = 60093,
		item_count = 1,
	},
}

advertisement[3] =
{
	advertisement_show = 0,   							--//是不是显示
	advertisement_type = 2,   							--//广告类型
	advertisement_name = "新春大闹冒险王国系列活动", 						--//广告的名字
	advertisement_begin_time = "2019.01.14.12.00.00",	--//开始时间
	advertisement_end_time = "2019.02.10.24.00.00", 	--//结束时间
	advertisement_pic_url = "version02\\系统\\推荐界面\\春节活动.tga",						--//广告类型图片
	advertisement_type_pic_url = "version02\\系统\\推荐界面\\丝带蓝色.tga", 				--//广告类型底板图片路径
	advertisement_type_pic = 1,						--//广告类型底板图片类型 0 正方形，1长方形
	advertisement_des1 = "80级以上角色可以参与。", 					--//大文字描述
	advertisement_des2 = "【春节团圆饭】\r【神仙到】\r【大闹冒险王国】\r新春怎么能少得了我们的猪蠢萌，“猪饭兜”借此机会来到了完美大陆，翻天覆地闹了一番。", 					--//小文字描述图片
	advertisement_goto_npc = 56646, 						--//根据类型去往的NPC
	advertisement_find_item = 0, 						--//更具类型打开的商城物品
	advertisement_dialog_name = "",						--//找不到命令用dialog名字
	advertisement_dlg_msg_id = 0,                       --//打开界面的命令，
	player_ReincarnationCount = 0,
	player_level = 1,
	player_RealmLevel = 0,
	["item_0"] = {    
        item_id = 41354,
		item_count = 1,
	},
	["item_1"] = {    
        item_id = 50262,
		item_count = 1,
	},
	["item_2"] = {    
        item_id = 50260,
		item_count = 1,
	},
}

advertisement[4] =
{
	advertisement_show = 0,   							--//是不是显示
	advertisement_type = 2,   							--//广告类型
	advertisement_name = "“完美轶事”称号冒险之旅", 						--//广告的名字
	advertisement_begin_time = "2019.01.09.08.00.00",	--//开始时间
	advertisement_end_time = "2019.01.16.08.00.00", 	--//结束时间
	advertisement_pic_url = "version02\\系统\\推荐界面\\阅历值.tga",						--//广告类型图片
	advertisement_type_pic_url = "version02\\系统\\推荐界面\\丝带蓝色.tga", 				--//广告类型底板图片路径
	advertisement_type_pic = 1,						--//广告类型底板图片类型 0 正方形，1长方形
	advertisement_des1 = "阅历值称号获取途径", 					--//大文字描述
	advertisement_des2 = "玩家可通过“完美轶事”查看自己在完美大陆中的阅历。\r凡达到一定阅历值的角色，均可获取相应称号。", 					--//小文字描述图片
	advertisement_goto_npc = 0, 						--//根据类型去往的NPC
	advertisement_find_item = 0, 						--//更具类型打开的商城物品
	advertisement_dialog_name = "",						--//找不到命令用dialog名字
	advertisement_dlg_msg_id = 0,                       --//打开界面的命令，
	player_ReincarnationCount = 0,
	player_level = 1,
	player_RealmLevel = 0,
	["item_0"] = {    
        item_id = 61519,
		item_count = 1,
	},
	["item_1"] = {    
        item_id = 61518,
		item_count = 1,
	},
}

advertisement[5] =
{
	advertisement_show = 0,   							--//是不是显示
	advertisement_type = 1,   							--//广告类型
	advertisement_name = "新春新衣", 						--//广告的名字
	advertisement_begin_time = "2019.01.16.08.00.00",	--//开始时间
	advertisement_end_time = "2019.02.26.08.00.00", 	--//结束时间
	advertisement_pic_url = "version02\\系统\\推荐界面\\新春时装.tga",						--//广告类型图片
	advertisement_type_pic_url = "version02\\系统\\推荐界面\\丝带绿色.tga", 				--//广告类型底板图片路径
	advertisement_type_pic = 1,						--//广告类型底板图片类型 0 正方形，1长方形
	advertisement_des1 = "时装商城新上架时装", 					--//大文字描述
	advertisement_des2 = "雪舞迎春时装（男、女）\r新春时装武器（包含神机、羽锋两个新职业）", 					--//小文字描述图片
	advertisement_goto_npc = 0, 						--//根据类型去往的NPC
	advertisement_find_item = 0, 						--//更具类型打开的商城物品
	advertisement_dialog_name = "Win_FashionShop",						--//找不到命令用dialog名字
	advertisement_dlg_msg_id = 0,                       --//打开界面的命令，
	player_ReincarnationCount = 0,
	player_level = 1,
	player_RealmLevel = 0,
	["item_0"] = {    
        item_id = 61595,
		item_count = 1,
	},
	["item_1"] = {    
        item_id = 61600,
		item_count = 1,
	},
	["item_2"] = {    
        item_id = 61607,
		item_count = 1,
	},
	["item_3"] = {    
        item_id = 61608,
		item_count = 1,
	},
}

advertisement[6] =
{
	advertisement_show = 0,   							--//是不是显示
	advertisement_type = 1,   							--//广告类型
	advertisement_name = "迎春骑宠", 						--//广告的名字
	advertisement_begin_time = "2019.01.16.08.00.00",	--//开始时间
	advertisement_end_time = "2019.02.26.08.00.00", 	--//结束时间
	advertisement_pic_url = "百宝阁\\2019年春节骑宠猪128.dds",						--//广告类型图片
	advertisement_type_pic_url = "version02\\系统\\推荐界面\\丝带绿色.tga", 				--//广告类型底板图片路径
	advertisement_type_pic = 0,						--//广告类型底板图片类型 0 正方形，1长方形
	advertisement_des1 = "猪猪送祥瑞", 					--//大文字描述
	advertisement_des2 = "新春节骑宠“猪来福”", 					--//小文字描述图片
	advertisement_goto_npc = 0, 						--//根据类型去往的NPC
	advertisement_find_item = 61604, 						--//更具类型打开的商城物品
	advertisement_dialog_name = "",						--//找不到命令用dialog名字
	advertisement_dlg_msg_id = 0,                       --//打开界面的命令，
	player_ReincarnationCount = 0,
	player_level = 1,
	player_RealmLevel = 0,
	["item_0"] = {    
        item_id = 61604,
		item_count = 1,
	},
}

advertisement[7] =
{
	advertisement_show = 0,   							--//是不是显示
	advertisement_type = 1,   							--//广告类型
	advertisement_name = "新年售卖凭证", 						--//广告的名字
	advertisement_begin_time = "2019.01.16.08.00.00",	--//开始时间
	advertisement_end_time = "2019.02.26.08.00.00", 	--//结束时间
	advertisement_pic_url = "百宝阁\\2019年春节摆摊猪128.dds",						--//广告类型图片
	advertisement_type_pic_url = "version02\\系统\\推荐界面\\丝带绿色.tga", 				--//广告类型底板图片路径
	advertisement_type_pic = 0,						--//广告类型底板图片类型 0 正方形，1长方形
	advertisement_des1 = "猪年快乐，大吉大利", 					--//大文字描述
	advertisement_des2 = "猪年售卖道具“萌猪聚宝”上架。", 					--//小文字描述图片
	advertisement_goto_npc = 0, 						--//根据类型去往的NPC
	advertisement_find_item = 61605, 						--//更具类型打开的商城物品
	advertisement_dialog_name = "",						--//找不到命令用dialog名字
	advertisement_dlg_msg_id = 0,                       --//打开界面的命令，
	player_ReincarnationCount = 0,
	player_level = 1,
	player_RealmLevel = 0,
	["item_0"] = {    
        item_id = 61605,
		item_count = 1,
	},
	["item_1"] = {    
        item_id = 61606,
		item_count = 1,
	},
}