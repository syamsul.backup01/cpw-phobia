--[[
王志昊 20170829
用于编辑特殊的NPC模型可以再一定范围内显示，而不消失

要是在地编中摆出来的NPC，各种参数详见地编！！！！！

！！注意：这个只能用于同一个场景中同一个NPC（模板ID相同）只有一个的情况！！

inst_id        地图编号
npc_id         NPC ID
pos_x          模型位置
pos_y
pos_z
dir          模型朝向
--]]
model_apper = {}

model_apper[1] =
{
	inst_id       =191,
	npc_id        =56507,
	show_distance =300,
	pos_x         =-195.41,
	pos_y         =45.54,
	pos_z         =-23.11,
	dir           =254,
}

model_apper[2] =
{
	inst_id       =191,
	npc_id        =56504,
	show_distance =300,
	pos_x         =-318.34,
	pos_y         =37.78,
	pos_z         =194.23,
	dir           =93,
}

model_apper[3] =
{
	inst_id       =191,
	npc_id        =56505,
	show_distance =300,
	pos_x         =-638.20,
	pos_y         =130.84,
	pos_z         =159.51,
	dir           =48,
}

model_apper[4] =
{
	inst_id       =191,
	npc_id        =56506,
	show_distance =300,
	pos_x         =-363.05,
	pos_y         =55.29,
	pos_z         =105.90,
	dir           =64,
}

model_apper[5] =
{
	inst_id       =191,
	npc_id        =57205,
	show_distance =380,
	pos_x         =-518.49,
	pos_y         =31.53,
	pos_z         =-109.93,
	dir           =15,
}

model_apper[6] =
{
	inst_id       =191,
	npc_id        =57445,
	show_distance =300,
	pos_x         =-221.06,
	pos_y         =32.42,
	pos_z         =-227.87,
	dir           =65,
}