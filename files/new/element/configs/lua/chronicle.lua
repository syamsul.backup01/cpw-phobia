--[[
FileName: chronicle.lua
Author: Mazongyang
Data: 2016.08.22
Description: 编年史配置文件 
             为解决海外项目语言适配问题,将编年史配置文件划分为两部分 
             除此文件外,另一配置文件为chronicle.stf
             所有在界面上需要显示的文字内容都配置在chronicle.stf文件中 
             .lua格式的文件必须使用utf-8编码 
             .stf必须使用Unicode编码 
             若将编码格式修改会导致程序无法正确读取文件
             修改编码格式可使用[记事本]打开文件,点击[另存为]
--]]

--编年史章节总表 
chronicle_chapter = {} 

--第一章  
chronicle_chapter["1"] = {
   
    --章标题 右值对应内容查看chronicle.stf文件 
    ["chapter_title"] = 1, 

    --第一节 
    ["section_1"] = {
       
       --节标题 右值对应内容查看chronicle.stf文件 
       ["section_title"] = 1001,

       --第一页 
       ["page_1"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1年1日.dds",
          ["text_1"] = 10001,
          ["text_2"] = 10301,
       },

       --第二页
       ["page_2"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1年2日.dds",
          ["text_1"] = 10002,
          ["text_2"] = 10302,
       },

       --第三页 
       ["page_3"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10003,
          ["text_2"] = 10004, 
       },

       --第四页
       ["page_4"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\225年.dds",
          ["text_1"] = 10005,
          ["text_2"] = 10006, 
       },

       --第五页 
       ["page_5"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\361年.dds",
          ["text_1"] = 10007,
          ["text_2"] = 10303,
       },

       --第六页
       ["page_6"] = {
          ["module_path"] = "historybook_sub2.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板2\\467年.dds",
          ["text_1"] = 10008,
          ["text_2"] = 10009, 
       },

       --第七页 
       ["page_7"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\天翼传奇.dds",
          ["text_1"] = 10010,
          ["text_2"] = 10011, 
       },

       --第八页
       ["page_8"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\687年.dds",
          ["text_1"] = 10012,
          ["text_2"] = 10304,
       },

       --第九页 
       ["page_9"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10013,
          ["text_2"] = 10014, 
       },

       --第十页
       ["page_10"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10015,
          ["text_2"] = 10016, 
       },

       --第十一页 
       ["page_11"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\重建家园.dds",
          ["text_1"] = 10017,
          ["text_2"] = 10305,
       },

       --第十二页
       ["page_12"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\875年.dds",
          ["text_1"] = 10018,
          ["text_2"] = 10306,
       },

       --第十三页 
       ["page_13"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\889年.dds",
          ["text_1"] = 10019,
          ["text_2"] = 10307,
       },

       --第十四页
       ["page_14"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10020,
          ["text_2"] = 10021, 
       },

       --第十五页 
       ["page_15"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\960年初.dds",
          ["text_1"] = 10022,
          ["text_2"] = 10308
       },

       --第十六页
       ["page_16"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\960年中.dds",
          ["text_1"] = 10023,
          ["text_2"] = 10309,
       },

       --第十七页 
       ["page_17"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10024,
          ["text_2"] = 10025, 
       },

       --第十八页
       ["page_18"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10026,
          ["text_2"] = 10027, 
       },

       --第十九页 
       ["page_19"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\苍力.dds",
          ["text_1"] = 10028,
          ["text_2"] = 10310,
       },

       --第二十页
       ["page_20"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\981年.dds",
          ["text_1"] = 10029,
          ["text_2"] = 10311,
       },
    },

    --第二节 
    ["section_2"] = {
     
       ["section_title"] = 1002,

       --第一页
       ["page_1"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1000年.dds",
          ["text_1"] = 10030,
          ["text_2"] = 10312,
       },

       --第二页
       ["page_2"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1017年.dds",
          ["text_1"] = 10031,
       },

       --第三页
       ["page_3"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10032,
          ["text_2"] = 10033, 
       },
       --第四页
       ["page_4"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1020年.dds",
          ["text_1"] = 10034,
       },

       --第五页
       ["page_5"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10035,
          ["text_2"] = 10036,
       },

       --第六页
       ["page_6"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\子纯.dds",
          ["text_1"] = 10037,
          ["text_2"] = 10038, 
       },

       --第七页
       ["page_7"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1044年.dds",
          ["text_1"] = 10039,
       },

       --第八页
       ["page_8"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\锦瑟.dds",
          ["text_1"] = 10040,
          ["text_2"] = 10041,
       },

       --第九页
       ["page_9"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1161年.dds",
          ["text_1"] = 10042,
       },

       --第十页
       ["page_10"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\872年.dds",         
          ["text_1"] = 10043,
          ["text_2"] = 10052,
       },

       --第十一页
       ["page_11"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1162年.dds",
          ["text_1"] = 10044,
          ["text_2"] = 10045,
       },

       --第十二页
       ["page_12"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1177年.dds",
          ["text_1"] = 10046,
       },

       --第十三页
       ["page_13"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1291年.dds",
          ["text_1"] = 10047,
          ["text_2"] = 10313,
       },

       --第十四页
       ["page_14"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\坠星湖.dds",         
          ["text_1"] = 10048,
          ["text_2"] = 10049,
       },

       --第十五页
       ["page_15"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1345年.dds",
          ["text_1"] = 10050, 
          ["text_2"] = 10051,
       },

       --第十六页
       ["page_16"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1366年.dds",
          ["text_1"] = 10053,       
       },

       --第十七页
       ["page_17"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1369年.dds",
          ["text_1"] = 10054,
          ["text_2"] = 10055,
       },

       --第十八页
       ["page_18"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\祖龙城.dds",
          ["text_1"] = 10056,
          ["text_2"] = 10317,
       },

       --第十九页
       ["page_19"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10057,
          ["text_2"] = 10058,
       },

       --第二十页
       ["page_20"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1374年.dds",
          ["text_1"] = 10059,
       },

       --第二十一页
       ["page_21"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1380年.dds",
          ["text_1"] = 10060,
          ["text_2"] = 10318,
       },

       --第二十二页
       ["page_22"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1406年.dds",
          ["text_1"] = 10061,
       },

       --第二十三页
       ["page_23"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1535年末.dds",
          ["text_1"] = 10062,
          ["text_2"] = 10319,
       },

       --第二十四页
       ["page_24"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10063,
          ["text_2"] = 10064, 
       },

       --第二十五页
       ["page_25"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1677年.dds",
          ["text_1"] = 10065,
       },

       --第二十六页
       ["page_26"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["text_1"] = 10066,
          ["text_2"] = 10067, 
       },

       --第二十七页
       ["page_27"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1701年.dds",
          ["text_1"] = 10068,
       },

       --第二十八页
       ["page_28"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\1999年.dds",
          ["text_1"] = 10069,
       },
    },
}

--第二章 
chronicle_chapter["2"] = {
    ["chapter_title"] = 2, 
   
    --第一节 
    ["section_1"] = {
     
       ["section_title"] = 1003,

       --第一页
       ["page_1"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\诗歌.dds",
          ["text_1"] = 10070,
       },

       --第二页
       ["page_2"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\3012年.dds",
          ["text_1"] = 10071,
          ["text_2"] = 10320,
       },

 
       --第三页
       ["page_3"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\3373年.dds",
          ["text_1"] = 10072,
          ["text_2"] = 10321,
       },

       --第四页
       ["page_4"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\3379年.dds",
          ["text_1"] = 10073,
       },

        --第五页
       ["page_5"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\3451年.dds",
          ["text_1"] = 10074,
          ["text_2"] = 10322,
       },

       --第六页
       ["page_6"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10075,
          ["text_2"] = 10076,         
       },

        --第七页
       ["page_7"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\3567年.dds",
          ["text_1"] = 10077,
       },

       --第八页
       ["page_8"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\3621年.dds",
          ["text_1"] = 10078,
          ["text_2"] = 10079,          
       },

        --第九页
       ["page_9"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\3656年.dds",          
          ["text_1"] = 10080, 
          ["text_2"] = 10323,     
       },

       --第十页
       ["page_10"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10081,
          ["text_2"] = 10082,          
       },

        --第十一页
       ["page_11"] = {
          ["module_path"] = "historybook_sub1.xml",  
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\3701年.dds",                
          ["text_1"] = 10083,                
       },

       --第十二页
       ["page_12"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\3702年.dds", 
          ["text_1"] = 10084,
          ["text_2"] = 10085,          
       },

        --第十三页
       ["page_13"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10086,
          ["text_2"] = 10087,          
       },

       --第十四页
       ["page_14"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\3810年.dds",
          ["text_1"] = 10088,
       },

        --第十五页
       ["page_15"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\3901年.dds",
          ["text_1"] = 10089,
          ["text_2"] = 10090,          
       },

       --第十六页
       ["page_16"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\3912年.dds",
          ["text_1"] = 10091,
          ["text_2"] = 10092,          
       },

        --第十七页
       ["page_17"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\3917年.dds",
          ["text_1"] = 10093,
          ["text_2"] = 10094,
       },
    },

    --第二节 
    ["section_2"] = {
     
       ["section_title"] = 1004,

       --第一页
       ["page_1"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\4012年冷月.dds",
          ["text_1"] = 10095,
          ["text_2"] = 10324,
       },

        --第二页
       ["page_2"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\4012年席尔瓦.dds",
          ["text_1"] = 10096,
          ["text_2"] = 10325,
       },

       --第三页
       ["page_3"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\4012年晓纯.dds",
          ["text_1"] = 10097,
          ["text_2"] = 10326,
       },

        --第四页
       ["page_4"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\4012年伊蒂丝.dds",
          ["text_1"] = 10098,
          ["text_2"] = 10327,
       },

       --第五页
       ["page_5"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\4021年.dds",
          ["text_1"] = 10099,
       },

        --第六页
       ["page_6"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\4023年.dds",
          ["text_1"] = 10100,
          ["text_2"] = 10328,
       },

       --第七页
       ["page_7"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\4105年.dds",
          ["text_1"] = 10101,
          ["text_2"] = 10102,          
       },

        --第八页
       ["page_8"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10103,
          ["text_2"] = 10104,          
       },

       --第九页
       ["page_9"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\胧族现世.dds",
          ["text_1"] = 10105,
          ["text_2"] = 10314,
       },

        --第十页
       ["page_10"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\4163年.dds",
          ["text_1"] = 10106,
          ["text_2"] = 10329,
       },

       --第十一页
       ["page_11"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\4165年.dds",
          ["text_1"] = 10107,
          ["text_2"] = 10330,
       },

        --第十二页
       ["page_12"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10108,
          ["text_2"] = 10109,
       },

       --第十三页
       ["page_13"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\4195年.dds",
          ["text_1"] = 10110,
          ["text_2"] = 10315,
       },

        --第十四页
       ["page_14"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\4217年.dds",
          ["text_1"] = 10111,
          ["text_2"] = 10331,
       },

       --第十五页
       ["page_15"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\4236年.dds",
          ["text_1"] = 10112,
          ["text_2"] = 10332,
       },

        --第十六页
       ["page_16"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10113,
          ["text_2"] = 10114,          
       },

       --第十七页
       ["page_17"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\4238年.dds",
          ["text_1"] = 10115,
          ["text_2"] = 10333,
       },

        --第十八页
       ["page_18"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\4240年.dds",
          ["text_1"] = 10116,
          ["text_2"] = 10117,          
       },

       --第十九页
       ["page_19"] = {
          ["module_path"] = "historybook_sub2.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板2\\4268年.dds",
          ["text_1"] = 10118,
          ["text_2"] = 10119,          
       },

        --第二十页
       ["page_20"] = {
          ["module_path"] = "historybook_sub2.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\古门结界.dds",
          ["text_1"] = 10120,
          ["text_2"] = 10121,         
       },

       --第二十一页
       ["page_21"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\白帝城.dds",
          ["text_1"] = 10122,
          ["text_2"] = 10123,
       },

        --第二十二页
       ["page_22"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\曙光殿.dds",
          ["text_1"] = 10124,
          ["text_2"] = 10125,
       },

         --第二十三页
       ["page_23"] = {
          ["module_path"] = "historybook_sub2.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板2\\奇乐冒险王国.dds",
          ["text_1"] = 10218,
          ["text_2"] = 10219,
       },

         --第二十四页
       ["page_24"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\十方神域奇观.dds",
          ["text_1"] = 10220,
          ["text_2"] = 10364,
       },
	   
	    --第二十五页
       ["page_25"] = {
          ["module_path"] = "historybook_sub2.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板2\\循环之门.dds",
          ["text_1"] = 10221,
          ["text_2"] = 10222,
       },
	   
	    --第二十六页
       ["page_26"] = {
          ["module_path"] = "historybook_sub1.xml",
		  ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\黑化明月姬.dds",
          ["text_1"] = 10223,
          ["text_2"] = 10365,
       },
	   
	    --第二十七页
       ["page_27"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\二十七.dds",
          ["text_1"] = 10224,
          ["text_2"] = 10225,
       },
	   
	    --第二十八页
       ["page_28"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\黑化重云.dds",
          ["text_1"] = 10226,
          ["text_2"] = 10366,
       },
	   
	   --第二十九页
       ["page_29"] = {
          ["module_path"] = "historybook_sub2.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板2\\二十九.dds",
          ["text_1"] = 10227,
          ["text_2"] = 10228,
       },
	   
	   --第三十页
       ["page_30"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\炮娘.dds",
          ["text_1"] = 10229,
          ["text_2"] = 10368,
       },
	   
	   --第三十一页
       ["page_31"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10230,
          ["text_2"] = 10231,
       },
	   
	    --第三十二页
       ["page_32"] = {
          ["module_path"] = "historybook_sub4.xml",
		  ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\场景原画01.dds",
          ["text_1"] = 10232,
          ["text_2"] = 10233,
       },
	   
	    --第三十三页
       ["page_33"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\剑盾.dds",
          ["text_1"] = 10234,
          ["text_2"] = 10367,
       },
	   
	   --第三十四页
       ["page_34"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\三十四.dds",
          ["text_1"] = 10235,
          ["text_2"] = 10236,
       },
    },
}

--第三章 
chronicle_chapter["3"] = {
    ["chapter_title"] = 3, 
   
    --第一节 
    ["section_1"] = {
     
       ["section_title"] = 1005,

       --第一页
       ["page_1"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\盘古开天.dds",
          ["text_1"] = 10126,
          ["text_2"] = 10127,         
       },

       --第二页
       ["page_2"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\诸神缘起.dds",
          ["text_1"] = 10128,
          ["text_2"] = 10129,          
       },

       --第三页
       ["page_3"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\法师.dds",
          ["text_1"] = 10130,
          ["text_2"] = 10334,
       },

       --第四页
       ["page_4"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\武侠.dds",
          ["text_1"] = 10131,
          ["text_2"] = 10335,
       },

       --第五页
       ["page_5"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\妖精.dds",
          ["text_1"] = 10132,
          ["text_2"] = 10336,
       },

       --第六页
       ["page_6"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\妖兽.dds",
          ["text_1"] = 10133,
          ["text_2"] = 10337,
       },

       --第七页
       ["page_7"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\羽灵.dds",
          ["text_1"] = 10134,
          ["text_2"] = 10338,
       },

       --第八页
       ["page_8"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\羽芒.dds",
          ["text_1"] = 10135,
          ["text_2"] = 10339,
       },

       --第九页
       ["page_9"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\刺客.dds",
          ["text_1"] = 10136,
          ["text_2"] = 10340,
       },

       --第十页
       ["page_10"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\巫师.dds",
          ["text_1"] = 10137,
          ["text_2"] = 10341,
       },

       --第十一页
       ["page_11"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\剑灵.dds",
          ["text_1"] = 10138,
          ["text_2"] = 10342,
       },

       --第十二页
       ["page_12"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\魅灵.dds",
          ["text_1"] = 10139,
          ["text_2"] = 10343,
       },

       --第十三页
       ["page_13"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\月仙.dds",
          ["text_1"] = 10140,
          ["text_2"] = 10344,
       },

       --第十四页
       ["page_14"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\夜影.dds",
          ["text_1"] = 10141,
          ["text_2"] = 10345,
       },

       --第十五页
       ["page_15"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10142,
          ["text_2"] = 10143,          
       },

       --第十六页
       ["page_16"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\仙魔传说.dds",
          ["text_1"] = 10144,
          ["text_2"] = 10346,
       },

       --第十七页
       ["page_17"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\血海之劫.dds",
          ["text_1"] = 10145, 
          ["text_2"] = 10347,       
       },

       --第十八页
       ["page_18"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10146,
          ["text_2"] = 10147,          
       },

       --第十九页
       ["page_19"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\天上人间.dds",
          ["text_1"] = 10148,
       },

       --第二十页
       ["page_20"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\星层割裂.dds",
          ["text_1"] = 10149,
       },

       --第二十一页
       ["page_21"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10150,
          ["text_2"] = 10151,          
       },

       --第二十二页
       ["page_22"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\怨灵的起源.dds",
          ["text_1"] = 10152,        
       },
    },

    --第二节 
    ["section_2"] = {
     
       ["section_title"] = 1006,

       --第一页
       ["page_1"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\度劫日.dds",
          ["text_1"] = 10153,
       },

       --第二页
       ["page_2"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\神寂之约.dds",
          ["text_1"] = 10154,
          ["text_2"] = 10155,
       },

       --第三页
       ["page_3"] = {
          ["module_path"] = "historybook_sub2.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板2\\诸神的弃儿.dds",
          ["text_1"] = 10156,
          ["text_2"] = 10157,          
       },
       
       --第四页
       ["page_4"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\天空之城.dds",
          ["text_1"] = 10158,
          ["text_2"] = 10159,          
       },

       --第五页
       ["page_5"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\神之子.dds",
          ["text_1"] = 10160,
          ["text_2"] = 10161,          
       },
       
       --第六页
       ["page_6"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\大圣城.dds",
          ["text_1"] = 10162,         
       },

       --第七页
       ["page_7"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10163,
          ["text_2"] = 10164,          
       },
       
       --第八页
       ["page_8"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10165,
          ["text_2"] = 10166,          
       },

       --第九页
       ["page_9"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\风云再起.dds",
          ["text_1"] = 10167,
       },
       
       --第十页
       ["page_10"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10168,
          ["text_2"] = 10169,          
       },

       --第十一页
       ["page_11"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10170,
          ["text_2"] = 10171,          
       },
       
       --第十二页
       ["page_12"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\伤麟森林血案.dds",
          ["text_1"] = 10172,
       },

       --第十三页
       ["page_13"] = {
          ["module_path"] = "historybook_sub2.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板2\\海外仙侠岛.dds",
          ["text_1"] = 10173,
          ["text_2"] = 10174,          
       },
       
       --第十四页
       ["page_14"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10175,
          ["text_2"] = 10176,          
       },

       --第十五页
       ["page_15"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\遮天之翼.dds",
          ["text_1"] = 10177,
       },
       
       --第十六页
       ["page_16"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10178,
          ["text_2"] = 10179,          
       },

       --第十七页
       ["page_17"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\青帝之都.dds",
          ["text_1"] = 10180,
          ["text_2"] = 10348,
       },
       
       --第十八页
       ["page_18"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10181,
          ["text_2"] = 10182,          
       },
      
       --第十九页
       ["page_19"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\黑帝之都.dds",          
          ["text_1"] = 10183,
          ["text_2"] = 10349,     
       },
      
       --第二十页
       ["page_20"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10184,
          ["text_2"] = 10185,          
       },

       --第二十一页
       ["page_21"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\白帝之城.dds",
          ["text_1"] = 10186,
          ["text_2"] = 10350,
       },
       
       --第二十二页
       ["page_22"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10187,
          ["text_2"] = 10188,          
       },

       --第二十三页
       ["page_23"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\黄帝之都.dds",
          ["text_1"] = 10189,
          ["text_2"] = 10351,
       },
       
       --第二十四页
       ["page_24"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10190,
          ["text_2"] = 10191,          
       },

       --第二十五页
       ["page_25"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\赤帝之都.dds",          
          ["text_1"] = 10192,
          ["text_2"] = 10352,         
       },
       
       --第二十六页
       ["page_26"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10193,
          ["text_2"] = 10194,          
       },

       --第二十七页
       ["page_27"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\众妙之门.dds",
          ["text_1"] = 10195,
          ["text_2"] = 10196,          
       },
       
       --第二十八页
       ["page_28"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10197,
          ["text_2"] = 10198,
       },
      
       --第二十九页
       ["page_29"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\飞天传奇.dds",
          ["text_1"] = 10199,
          ["text_2"] = 10316,
       },

       --第三十页
       ["page_30"] = {
          ["module_path"] = "historybook_sub4.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板4\\寻梦港.dds",
          ["text_1"] = 10200,
          ["text_2"] = 10201,          
       },
    },

    --第三节 
    ["section_3"] = {
     
       ["section_title"] = 1007,

       --第一页
       ["page_1"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\万化城.dds",
          ["text_1"] = 10202,
          ["text_2"] = 10353,
       },

       --第二页
       ["page_2"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\剑仙城.dds",
          ["text_1"] = 10203,
          ["text_2"] = 10354,
       },

       --第三页
       ["page_3"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\积羽城.dds",
          ["text_1"] = 10204, 
          ["text_2"] = 10355,      
       },
       
       --第四页
       ["page_4"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\灵犀城.dds",
          ["text_1"] = 10205,
          ["text_2"] = 10356,
       },

       --第五页
       ["page_5"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\惊涛城.dds",
          ["text_1"] = 10206,
          ["text_2"] = 10357,
       },
       
       --第六页
       ["page_6"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\赢霄城.dds",
          ["text_1"] = 10207,
          ["text_2"] = 10358,
       },

       --第七页
       ["page_7"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\仙界.dds",
          ["text_1"] = 10208,
          ["text_2"] = 10359,
       },
       
       --第八页
       ["page_8"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\魔界.dds",
          ["text_1"] = 10209,
          ["text_2"] = 10360,
       },

       --第九页
       ["page_9"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10210,
          ["text_2"] = 10211,          
       },
       
       --第十页
       ["page_10"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\复制者.dds",
          ["text_1"] = 10212,
          ["text_2"] = 10361,
       },

       --第十一页
       ["page_11"] = {
          ["module_path"] = "historybook_sub2.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板2\\魔法学院.dds",
          ["text_1"] = 10213,
          ["text_2"] = 10214,          
       },

       --第十二页
       ["page_12"] = {
          ["module_path"] = "historybook_sub3.xml",
          ["text_1"] = 10215,
          ["text_2"] = 10216,          
       },
       
       --第十三页
       ["page_13"] = {
          ["module_path"] = "historybook_sub1.xml",
          ["picture_path"] = "version02\\编年史\\编年史插画\\模板1\\魔女祭坛.dds",
          ["text_1"] = 10217, 
          ["text_2"] = 10362,     
      },
    },
}