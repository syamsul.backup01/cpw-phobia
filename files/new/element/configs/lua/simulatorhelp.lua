--[[
FileName: chronicle.lua
Author: Mazongyang
Data: 2017.04.24
Description: 模拟器帮助界面配置文件
             .lua格式的文件须使用utf-8编码 
--]]

--帮助界面配置表 
simulator_help = {} 


--第一个界面 
simulator_help[1] = 
{
    ["dlg_name"] = "Win_Simulator_HelpAstrolabe",  --名字
    ["item_id"] = {
        47379,     -- 物品1 id
        47493,     -- 物品2 id 
        47376,
        47494,
        47377,
        47501,
        47502,
        47378,
        47452,
        47453,
        47495,
        47496,
        47497,
        47499,
        47500,
    }
}


simulator_help[2] = 
{
    ["dlg_name"] ="Win_Simulator_HelpBook",  --名字
    ["item_id"] = {
        36676,
        36675,  
        36343,
        37012,
        36343,
        37012,
        44005,
        43997,
        18185,
        17810,
    }
}

simulator_help[3] = 
{
    ["dlg_name"] = "Win_Simulator_HelpCard",  --名字
    ["item_id"] = {
        42202,     -- 物品1 id
        48353, 
        41070,
        42397,
        51870,
        51871,
        41072,
        42172,  
    }
}

simulator_help[4] = 
{
    ["dlg_name"] = "Win_Simulator_HelpRune",  --名字
    ["item_id"] = {
        54332,     -- 物品1 id
        54333, 
        54336,
        52175,
        52185,
        52195,
        52205,
        52215, 
        52178,
        52188,
        52198,
        52208,
        52218, 
    }
}